package bsu.cs402.siriusgomes.designpatternsapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import bsu.cs402.siriusgomes.Constants.Constants;
import bsu.cs402.siriusgomes.controller.PersistenceManager;
import bsu.cs402.siriusgomes.model.DesignPattern;

public class DesignPatternsViewer extends ActionBarActivity {

    PersistenceManager persistenceManager;
    DesignPattern startDesignPattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_patterns_viewer);

        CheckBox isFavorite = (CheckBox) findViewById(R.id.checkBoxFavoriteViewer);

    }

    private void reloadViewer() {
        if (persistenceManager == null)
        persistenceManager = new PersistenceManager(this);

        if (persistenceManager.getListSize() > 0) {

            if ((startDesignPattern = persistenceManager.loadState()) == null) {
                int number = new Random().nextInt() % persistenceManager.getListSize();
                startDesignPattern = persistenceManager.getDesignPattern(Math.abs(number));
            }
            TextView name = (TextView) findViewById(R.id.designPatternNameField);
            TextView category = (TextView) findViewById(R.id.designPatternCategoryField);
            TextView description = (TextView) findViewById(R.id.designPatternDescriptionField);
            CheckBox isFavorite = (CheckBox) findViewById(R.id.checkBoxFavoriteViewer);

               // Listener to save the change when you press the checkbox to favorite
       /*     isFavorite.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    persistenceManager.addDesignPattern(startDesignPattern);
                }
            });
*/
            name.setText(startDesignPattern.getName());
            category.setText(startDesignPattern.getCategory());
            description.setText(startDesignPattern.getDescription());
            isFavorite.setChecked(startDesignPattern.isFavorite());

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //reloadViewer();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadViewer();
    }

    @Override
    protected void onDestroy() {
        persistenceManager.saveState(startDesignPattern);
        super.onDestroy();
    }



    public void checkBoxOnClick(View view) {
        startDesignPattern.setFavorite(!startDesignPattern.isFavorite());
        persistenceManager.editDesignPattern(startDesignPattern);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_design_patterns_viewer, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (data.getBooleanExtra(Constants.returnData, false)) {
                Toast.makeText(DesignPatternsViewer.this, getString(R.string.message_save), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DesignPatternsViewer.this, getString(R.string.message_error), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        switch (id) {
            case R.id.add_new:
                Intent designPatternCreator = new Intent(DesignPatternsViewer.this, DesignPatternEditor.class);
                startActivityForResult(designPatternCreator, Constants.designsPatternEditor);
                return true;
            case R.id.list_all:
                Intent designPatternList = new Intent(DesignPatternsViewer.this, DesignPatternList.class);
                startActivity(designPatternList);
                return true;
            case R.id.edit_actual:
                Intent designPatternEditor = new Intent(DesignPatternsViewer.this, DesignPatternEditor.class);
                designPatternEditor.putExtra("DesignPattern", startDesignPattern);
                startActivityForResult(designPatternEditor, 0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
