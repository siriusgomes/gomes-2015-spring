package bsu.cs402.siriusgomes.Constants;

/**
 * Created by Sírius on 2/3/2015.
 */
public class Constants {

    public static final String fileName = "DesignPatterns.json";
    public static final String fileSaveStateName = "saveState.json";
    public static final int designsPatternEditor = 1;
    public static final String returnData = "RETURN_DATA";

    // JSON Keys name
    public static final String keyName = "name";
    public static final String keyDescription = "description";
    public static final String keyCategory = "category";
    public static final String keyFavorite = "favorite";


}
