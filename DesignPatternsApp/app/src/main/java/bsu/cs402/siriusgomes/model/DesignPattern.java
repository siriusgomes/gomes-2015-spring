package bsu.cs402.siriusgomes.model;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;

import bsu.cs402.siriusgomes.Constants.Constants;
import bsu.cs402.siriusgomes.designpatternsapp.R;

/**
 * Created by Sírius on 2/3/2015.
 */
public class DesignPattern implements Serializable {

    private String name;
    private String category;
    private String description;
    private boolean isFavorite;

    @Override
    public String toString() {
        return name + " - " + category + " - " + description + (isFavorite?" - Favorite": "");
    }

    public DesignPattern(JSONObject json) {

        try {
            setName(json.getString(Constants.keyName));
            setCategory(json.getString(Constants.keyCategory));
            setDescription(json.getString(Constants.keyDescription));
            setFavorite(json.getBoolean(Constants.keyFavorite));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DesignPattern(String name, String category, String description, boolean isFavorite) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.isFavorite = isFavorite;
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Constants.keyName, name);
            jsonObject.put(Constants.keyCategory, category);
            jsonObject.put(Constants.keyDescription, description);
            jsonObject.put(Constants.keyFavorite, isFavorite);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object another) {
        if (another instanceof DesignPattern) {
            if (this.name.equals(((DesignPattern)another).getName()))
                return true;
            else
                return false;
        }

        return super.equals(another);
    }
}
