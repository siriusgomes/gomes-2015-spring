package bsu.cs402.siriusgomes.controller;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import bsu.cs402.siriusgomes.Constants.Constants;
import bsu.cs402.siriusgomes.model.DesignPattern;

/**
 * Created by Sírius on 2/3/2015.
 * To populate with a small list of design patterns, run save(1).
 * To populate with a huge list of design patterns, run save(2).
 * The app will create a small list if you delete all the data and restart the app.
 */
public class PersistenceManager {

    private List<DesignPattern> list = new ArrayList<DesignPattern>();
    private Context context;


    public PersistenceManager(Context context) {
        this.context = context;
        load();
        if (getListSize() == 0) {
            save(1);
            //save(2);
        }
    }

    public List<DesignPattern> getList() {
        return list;
    }

    public DesignPattern getDesignPattern(int number) {
        return list.get(number);
    }

    public boolean removeDesignPattern(int number) {
        if (list.remove(number) != null) {
            save(0);
            return true;
        }
        return false;
    }

    public boolean removeDesignPattern(DesignPattern designPattern) {
        if (list.remove(list.indexOf(designPattern)) != null) {
            save(0);
            return true;
        }
        return false;
    }

    public boolean addDesignPattern(DesignPattern designPattern) {
        list.add(designPattern);
        return save(0);
    }


    // I know that these 2 functions above are bad, but It's to hard do debug on android, so I just did this "bad programming" ..
    // Lists should be easier to use, it was doubling the data every time, even with the equals() implementation of DesignPattern..
    // I was not sure if i should implement equals or hashCode too, whatever! It's working in this way..
    public boolean editDesignPattern(DesignPattern designPattern) {
        int indexOfOld = list.indexOf(designPattern);
        list.remove(designPattern);
        list.add(indexOfOld, designPattern);
        return save(0);
    }

    public int getListSize() {
        return list.size();
    }

    private boolean save(int option) {
        boolean isDone = false;
        try {
            JSONArray jsonArray = new JSONArray();
            for (DesignPattern dp : list) {
                jsonArray.put(dp.toJSON());
            }

            OutputStream out = context.openFileOutput(Constants.fileName, Context.MODE_PRIVATE);
            Writer writer = new OutputStreamWriter(out);

            if (option == 0) {
                writer.write(jsonArray.toString());
            } else if (option == 1) {
                writer.write(jsonLight);
            } else if (option == 2) {
                writer.write(jsonHeavy);
            }

            writer.close();
            isDone = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDone;
    }

    public void saveState(DesignPattern designPattern) {
        try {
            OutputStream out = context.openFileOutput(Constants.fileSaveStateName, Context.MODE_PRIVATE);
            Writer writer = new OutputStreamWriter(out);
            JSONObject obj = new JSONObject();
            writer.write(designPattern.toJSON().toString());
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public DesignPattern loadState() {
        DesignPattern designPattern = null;
        try {
            InputStream is = context.openFileInput(Constants.fileSaveStateName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder strBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                strBuilder.append(line);
            }

            //JSONArray jsonArray = (JSONArray) new JSONTokener(strBuilder.toString()).nextValue();
            JSONObject jsonObj = new JSONObject(strBuilder.toString());
            designPattern = new DesignPattern(jsonObj);

            context.deleteFile(Constants.fileSaveStateName);

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return designPattern;
    }

    private boolean load() {
        boolean isDone = false;
        try {
            InputStream is = context.openFileInput(Constants.fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder strBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                strBuilder.append(line);
            }

            //JSONArray jsonArray = (JSONArray) new JSONTokener(strBuilder.toString()).nextValue();
            JSONArray jsonArray = new JSONArray(strBuilder.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                list.add(new DesignPattern(jsonArray.getJSONObject(i)));
            }
            br.close();
            isDone = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDone;
    }

    String jsonHeavy = "[{\"name\":\"Abstract factory\",\"description\":\"Provide an interface for creating families of related or dependent objects without specifying their concrete classes.\",\"category\":\"Creational\",\"favorite\":\"true\"},\n" +
            "{\"name\":\"Builder\",\"description\":\"Separate the construction of a complex object from its representation, allowing the same construction process to create various representations.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Factory method\",\"description\":\"Define an interface for creating a single object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses (dependency injection[15]).\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Lazy initialization\",\"description\":\"Tactic of delaying the creation of an object, the calculation of a value, or some other expensive process until the first time it is needed. This pattern appears in the GoF catalog as \",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Multiton\",\"description\":\"Ensure a class has only named instances, and provide global point of access to them.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Object pool\",\"description\":\"Avoid expensive acquisition and release of resources by recycling objects that are no longer in use. Can be considered a generalisation of connection pool and thread pool patterns.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Prototype\",\"description\":\"Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Resource acquisition is initialization\",\"description\":\"Ensure that resources are properly released by tying them to the lifespan of suitable objects.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Singleton\",\"description\":\"Ensure a class has only one instance, and provide a global point of access to it.\",\"category\":\"Creational\",\"favorite\":\"true\"},\n" +
            "{\"name\":\"Adapter or Wrapper or Translator.\",\"description\":\"Convert the interface of a class into another interface clients expect. An adapter lets classes work together that could not otherwise because of incompatible interfaces. The enterprise integration pattern equivalent is the translator.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Bridge\",\"description\":\"Decouple an abstraction from its implementation allowing the two to vary independently.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Composite\",\"description\":\"Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Decorator\",\"description\":\"Attach additional responsibilities to an object dynamically keeping the same interface. Decorators provide a flexible alternative to subclassing for extending functionality.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Facade\",\"description\":\"Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Flyweight\",\"description\":\"Use sharing to support large numbers of similar objects efficiently.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Front Controller\",\"description\":\"The pattern relates to the design of Web applications. It provides a centralized entry point for handling requests.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Module\",\"description\":\"Group several related elements, such as classes, singletons, methods, globally used, into a single conceptual entity.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Proxy\",\"description\":\"Provide a surrogate or placeholder for another object to control access to it.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Twin [17]\",\"description\":\"Twin allows to model multiple inheritance in programming languages that do not support this feature.\",\"category\":\"Structural\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Blackboard\",\"description\":\"Generalized observer, which allows multiple readers and writers. Communicates information system-wide.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Chain of responsibility\",\"description\":\"Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Command\",\"description\":\"Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Interpreter\",\"description\":\"Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Iterator\",\"description\":\"Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Mediator\",\"description\":\"Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Memento\",\"description\":\"Without violating encapsulation, capture and externalize an object's internal state allowing the object to be restored to this state later.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Null object\",\"description\":\"Avoid null references by providing a default object.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Observer orPublish/subscribe\",\"description\":\"Define a one-to-many dependency between objects where a state change in one object results in all its dependents being notified and updated automatically.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Servant\",\"description\":\"Define common functionality for a group of classes\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Specification\",\"description\":\"Recombinable business logic in a Boolean fashion\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"State\",\"description\":\"Allow an object to alter its behavior when its internal state changes. The object will appear to change its class.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Strategy\",\"description\":\"Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Template method\",\"description\":\"Define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Visitor\",\"description\":\"Represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes of the elements on which it operates.\",\"category\":\"Behavioral\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Active Object\",\"description\":\"Decouples method execution from method invocation that reside in their own thread of control. The goal is to introduce concurrency, by using asynchronous method invocation and a scheduler for handling requests.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Balking\",\"description\":\"Only execute an action on an object when the object is in a particular state.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Binding properties\",\"description\":\"Combining multiple observers to force properties in different objects to be synchronized or coordinated in some way.[19]\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Double-checked locking\",\"description\":\"Reduce the overhead of acquiring a lock by first testing the locking criterion (the 'lock hint') in an unsafe manner; only if that succeeds does the actual lock proceed.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"\",\"description\":\"Can be unsafe when implemented in some language/hardware combinations. It can therefore sometimes be considered an anti-pattern.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Event-based asynchronous\",\"description\":\"Addresses problems with the asynchronous pattern that occur in multithreaded programs.[20]\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Guarded suspension\",\"description\":\"Manages operations that require both a lock to be acquired and a precondition to be satisfied before the operation can be executed.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Join\",\"description\":\"Join-patterns provides a way to write concurrent, parallel and distributed programs by message passing. Compared to the use of threads and locks, this is a high level programming model.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Lock\",\"description\":\"One thread puts a \",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Messaging design pattern (MDP)\",\"description\":\"Allows the interchange of information (i.e. messages) between components and applications.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Monitor object\",\"description\":\"An object whose methods are subject to mutual exclusion, thus preventing multiple objects from erroneously trying to use it at the same time.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Reactor\",\"description\":\"A reactor object provides an asynchronous interface to resources that must be handled synchronously.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Read-write lock\",\"description\":\"Allows concurrent read access to an object, but requires exclusive access for write operations.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Scheduler\",\"description\":\"Explicitly control when threads may execute single-threaded code.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Thread pool\",\"description\":\"A number of threads are created to perform a number of tasks, which are usually organized in a queue. Typically, there are many more tasks than threads. Can be considered a special case of the object pool pattern.\",\"category\":\"Concurrency\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Thread-specific storage\",\"description\":\"Static or \",\"category\":\"Concurrency\",\"favorite\":\"false\"}]";

    String jsonLight = "[{\"name\":\"Abstract factory\",\"description\":\"Provide an interface for creating families of related or dependent objects without specifying their concrete classes.\",\"category\":\"Creational\",\"favorite\":\"true\"},\n" +
            "{\"name\":\"Builder\",\"description\":\"Separate the construction of a complex object from its representation, allowing the same construction process to create various representations.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Factory method\",\"description\":\"Define an interface for creating a single object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses (dependency injection[15]).\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Lazy initialization\",\"description\":\"Tactic of delaying the creation of an object, the calculation of a value, or some other expensive process until the first time it is needed. This pattern appears in the GoF catalog as \",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Multiton\",\"description\":\"Ensure a class has only named instances, and provide global point of access to them.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Object pool\",\"description\":\"Avoid expensive acquisition and release of resources by recycling objects that are no longer in use. Can be considered a generalisation of connection pool and thread pool patterns.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Prototype\",\"description\":\"Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Resource acquisition is initialization\",\"description\":\"Ensure that resources are properly released by tying them to the lifespan of suitable objects.\",\"category\":\"Creational\",\"favorite\":\"false\"},\n" +
            "{\"name\":\"Singleton\",\"description\":\"Ensure a class has only one instance, and provide a global point of access to it.\",\"category\":\"Creational\",\"favorite\":\"true\"}]";

}
