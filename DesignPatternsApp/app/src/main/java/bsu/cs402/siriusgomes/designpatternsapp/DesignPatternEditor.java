package bsu.cs402.siriusgomes.designpatternsapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import bsu.cs402.siriusgomes.Constants.Constants;
import bsu.cs402.siriusgomes.controller.PersistenceManager;
import bsu.cs402.siriusgomes.model.DesignPattern;


public class DesignPatternEditor extends ActionBarActivity {

    PersistenceManager persistenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_pattern_editor);

        if (persistenceManager == null)
        persistenceManager = new PersistenceManager(this);

        populateCategorySpinner();
        final EditText editTextName = (EditText) findViewById(R.id.editTextName);
        final EditText editTextDescription = (EditText) findViewById(R.id.editTextDescription);
        final Spinner spinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
        final CheckBox checkBoxIsFavorite = (CheckBox) findViewById(R.id.checkBoxFavorite);

        final DesignPattern dp = (DesignPattern) getIntent().getSerializableExtra("DesignPattern");
        // It means that it is an Edit Operation..
        if (dp != null) {
            editTextName.setText(dp.getName());
            editTextName.setEnabled(false);
            editTextDescription.setText(dp.getDescription());
            SelectSpinnerItemByValue(spinnerCategory, dp.getCategory());
            checkBoxIsFavorite.setChecked(dp.isFavorite());
        }

        Button saveButton = (Button) findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DesignPattern newDesignPattern = new DesignPattern(editTextName.getText().toString(), spinnerCategory.getSelectedItem().toString(), editTextDescription.getText().toString(), checkBoxIsFavorite.isChecked());

                if ((dp != null && persistenceManager.editDesignPattern(newDesignPattern)) || persistenceManager.addDesignPattern(newDesignPattern)) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constants.returnData, true);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
                else {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constants.returnData, false);
                    setResult(RESULT_CANCELED, returnIntent);
                    finish();
                }

            }
        });
/*
        final TextView textview = (TextView) findViewById(R.id.textView);
        Button btnReadJson = (Button) findViewById(R.id.button2);
        btnReadJson.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DesignPatternEditor.this, "Setting text...", Toast.LENGTH_SHORT).show();

                try {
                    InputStream is = openFileInput(filename);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    StringBuilder strBuilder = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        strBuilder.append(line);
                    }

                    JSONObject jsonObj = new JSONObject(strBuilder.toString());

                    textview.setText(jsonObj.get(keyName).toString());


                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
*/
    }

    private void SelectSpinnerItemByValue(Spinner spinner, Object value)
    {
        int position = 0;
        while (!spinner.getItemAtPosition(position).toString().equals(value.toString())) {
            position++;
        }
        spinner.setSelection(position);
    }

    private void populateCategorySpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinnerCategory);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.design_pattern_categories, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_design_pattern_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
