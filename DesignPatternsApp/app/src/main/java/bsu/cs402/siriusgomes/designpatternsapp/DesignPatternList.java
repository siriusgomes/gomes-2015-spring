package bsu.cs402.siriusgomes.designpatternsapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import bsu.cs402.siriusgomes.controller.PersistenceManager;
import bsu.cs402.siriusgomes.designpatternsapp.R;
import bsu.cs402.siriusgomes.model.DesignPattern;

import static android.widget.AdapterView.AdapterContextMenuInfo;


public class DesignPatternList extends ActionBarActivity {

    private PersistenceManager persistenceManager;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_pattern_list);

        if (persistenceManager == null)
            persistenceManager = new PersistenceManager(this);

        listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter<DesignPattern> adapter = new ArrayAdapter<DesignPattern>(this, android.R.layout.simple_list_item_1, persistenceManager.getList());
        listView.setAdapter(adapter);

        //registerForContextMenu(listView);




        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(DesignPatternList.this, "Deleting : " + persistenceManager.getDesignPattern(position)  , Toast.LENGTH_SHORT).show();
                //persistenceManager.removeDesignPattern(position);
                removeElementListView(position);
                return false;
            }
        });

/*
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(DesignPatternList.this, "" + parent.toString() + "-" + view.toString() + "-" + position + "-" + id, Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    private void removeElementListView(int id) {
        persistenceManager.removeDesignPattern(id);
        ArrayAdapter<DesignPattern> adapter = new ArrayAdapter<DesignPattern>(this, android.R.layout.simple_list_item_1, persistenceManager.getList());
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_design_pattern_list, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        switch(item.getItemId()){
            case R.id.menuDelete:

                break;

        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
