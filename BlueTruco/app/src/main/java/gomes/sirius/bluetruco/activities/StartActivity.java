package gomes.sirius.bluetruco.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import gomes.sirius.bluetruco.R;
import gomes.sirius.bluetruco.constants.Constants;
import gomes.sirius.bluetruco.controller.BlueTrucoSocket;
import gomes.sirius.bluetruco.controller.GameRules;
import gomes.sirius.bluetruco.controller.Table;
import gomes.sirius.bluetruco.model.Card;
import gomes.sirius.bluetruco.model.Player;
import gomes.sirius.bluetruco.model.Team;


public class StartActivity extends Activity {

    private Table table;

    private Player player1;
    private Player player2;
    private String gameMode;

    private boolean trucoAsked = false;

    //Cards in the table..
    private ImageView card1Player1;
    private ImageView card2Player1;
    private ImageView card3Player1;
    private ImageView card1Player2;
    private ImageView card2Player2;
    private ImageView card3Player2;
    private ImageView turnedCardImageView;
    private ImageView genericCardImageView;
    private Button buttonTruco;
    private Button buttonDecline;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_start);

        // First let's define the game mode...
        gameMode = getIntent().getStringExtra(Constants.GAME_MODE);

        Log.d("StartActivity - onCreate", "Starting activity in mode " + gameMode);

        if (getIntent().getStringExtra(Constants.GAME_MODE).equals(Constants.SINGLE_PLAYER_MODE)) {
           startSinglePlayer();
        }
        else if (getIntent().getStringExtra(Constants.GAME_MODE).equals(Constants.MULTI_PLAYER_MODE_SERVER)) {
            //startMultiPlayerServer();


            ServerSocketInputHandler serverSocketInputHandler = new ServerSocketInputHandler();
            serverSocketInputHandler.execute();

        }
        else if (getIntent().getStringExtra(Constants.GAME_MODE).equals(Constants.MULTI_PLAYER_MODE_CLIENT)) {
            //startMultiPlayerServer();


            ClientSocketInputHandler clientSocketInputHandler = new ClientSocketInputHandler();
            clientSocketInputHandler.execute();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            LocalBroadcastManager.getInstance(StartActivity.this).unregisterReceiver(cpuCall);

            LocalBroadcastManager.getInstance(StartActivity.this).unregisterReceiver(newRoundCall);

            LocalBroadcastManager.getInstance(StartActivity.this).unregisterReceiver(cpuAnswerTruco);


            if (BlueTrucoSocket.socket != null) {
                BlueTrucoSocket.socket.close();
            }
            else {
                LocalBroadcastManager.getInstance(StartActivity.this).unregisterReceiver(socketInput);
            }
            BlueTrucoSocket.socket = null;
            BlueTrucoSocket.bufferOutput.clear();
            BlueTrucoSocket.bufferInput.clear();
        } catch (Exception e) {
            Log.e("Closing Socket Error", e.getMessage());
        }
        Log.d("StartActivity - onDestroy", "Unregistered LocalBroadcastReceivers cpuCall, cpuAnswerTruco and newRoundCall. Closed Socket");
    }

    private void startMultiPlayerClient(JSONObject initialPack) {

        try {

            Log.d("startMultiPlayerClient", "Starting multi player game as client ");

            Player playerFactor = new Player();

            // Instantiate two players...
            player1 = playerFactor.fromJsonObject((JSONObject) initialPack.get(Constants.JSON_PLAYER1));
            player2 = playerFactor.fromJsonObject((JSONObject) initialPack.get(Constants.JSON_PLAYER2));

            TextView team1 = (TextView) findViewById(R.id.team1TextView);
            TextView team2 = (TextView) findViewById(R.id.team2TextView);
            TextView team1Rounds = (TextView) findViewById(R.id.team1RoundsTextView);
            TextView team2Rounds = (TextView) findViewById(R.id.team2RoundsTextView);
            TextView team1TotalPoints = (TextView) findViewById(R.id.team1PointsTextView);
            TextView team2TotalPoints = (TextView) findViewById(R.id.team2PointsTextView);
            buttonTruco = (Button) findViewById(R.id.buttonTrucoSixNineTwelve);
            buttonDecline = (Button) findViewById(R.id.buttonDecline);

            team1.setText(player1.getName());
            team2.setText(player2.getName());
            team1Rounds.setText(initialPack.get(Constants.JSON_SCORE_ROUND_TEAM1).toString());
            team2Rounds.setText(initialPack.get(Constants.JSON_SCORE_ROUND_TEAM2).toString());
            team1TotalPoints.setText(initialPack.get(Constants.JSON_SCORE_TEAM1).toString());
            team2TotalPoints.setText(initialPack.get(Constants.JSON_SCORE_TEAM2).toString());

            // New table obj.
            table = new Table(player1, player2, this, team1Rounds, team1TotalPoints, team2Rounds, team2TotalPoints, Constants.MULTI_PLAYER_MODE_CLIENT, buttonTruco, buttonDecline);
            table.setTurnedCard(new Card().fromJsonObject((JSONObject) initialPack.get(Constants.JSON_CARD_TURNED)));

            // Let's start the game with the real player..
            table.setNextPlayer(player1.getTeam());

            table.setListenerButtonTruco(3);
            table.hideDeclineButton();

            startNewRound(2);
        } catch (Exception e) {
            Log.d("StartMultiPlayerClient", e.getMessage());
        }
    }

    private void startMultiPlayerServer() {
        Log.d("startMultiPlayerServer", "Starting multi player game as server ");
        // Instanciate two players...

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_NAME, MODE_PRIVATE);
        String userName = sharedPreferences.getString(Constants.USER_NAME, "Server");

        player1 = new Player(userName,1);
        //player2 = new Player("Bluetooth",1);

        // Set the method to receive the information that the round has ended. This will call the method startNewRound.
        LocalBroadcastManager.getInstance(StartActivity.this).registerReceiver(newRoundCall, new IntentFilter(Constants.INTENT_BROADCAST_NEXT_ROUND));

        TextView team1 = (TextView) findViewById(R.id.team1TextView);
        TextView team2 = (TextView) findViewById(R.id.team2TextView);
        TextView team1Rounds = (TextView) findViewById(R.id.team1RoundsTextView);
        TextView team2Rounds = (TextView) findViewById(R.id.team2RoundsTextView);
        TextView team1TotalPoints = (TextView) findViewById(R.id.team1PointsTextView);
        TextView team2TotalPoints = (TextView) findViewById(R.id.team2PointsTextView);
        buttonTruco = (Button) findViewById(R.id.buttonTrucoSixNineTwelve);
        buttonDecline = (Button) findViewById(R.id.buttonDecline);

        team1.setText(player1.getName());
        team2.setText(player2.getName());

        // New table obj.
        table = new Table(player1, player2, this, team1Rounds, team1TotalPoints, team2Rounds, team2TotalPoints, Constants.MULTI_PLAYER_MODE_SERVER, buttonTruco, buttonDecline);

        // Let's start the game with the server..
        table.setNextPlayer(player1.getTeam());

        startNewRound(1);
        sendClientCardsOfRound(true);
    }

    private void startSinglePlayer() {
        Log.d("startSinglePlayer", "Starting single player game");

        // Set the method to receive the information that the user already had played. This will call the AI to play.
        LocalBroadcastManager.getInstance(StartActivity.this).registerReceiver(cpuCall, new IntentFilter(Constants.INTENT_BROADCAST_NEXT_PLAYER));

        // Set the method to receive the information that the round has ended. This will call the method startNewRound.
        LocalBroadcastManager.getInstance(StartActivity.this).registerReceiver(newRoundCall, new IntentFilter(Constants.INTENT_BROADCAST_NEXT_ROUND));

         // Set the method to decide if the AI will accept truco or not.
        LocalBroadcastManager.getInstance(StartActivity.this).registerReceiver(cpuAnswerTruco, new IntentFilter(Constants.INTENT_BROADCAST_ANSWER_TRUCO));

        Log.d("StartActivity", "Registred cpuCall and newRoundCall broadcastmanagers to handle cpu and new round calls.");

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_NAME, MODE_PRIVATE);
        String userName = sharedPreferences.getString(Constants.USER_NAME, "Server");

        // Instanciate two players...
        player1 = new Player(userName,1);
        player2 = new Player("CPU",2);

        TextView team1 = (TextView) findViewById(R.id.team1TextView);
        TextView team2 = (TextView) findViewById(R.id.team2TextView);
        TextView team1Rounds = (TextView) findViewById(R.id.team1RoundsTextView);
        TextView team2Rounds = (TextView) findViewById(R.id.team2RoundsTextView);
        TextView team1TotalPoints = (TextView) findViewById(R.id.team1PointsTextView);
        TextView team2TotalPoints = (TextView) findViewById(R.id.team2PointsTextView);
        buttonTruco = (Button) findViewById(R.id.buttonTrucoSixNineTwelve);
        buttonDecline = (Button) findViewById(R.id.buttonDecline);

        team1.setText(player1.getName());
        team2.setText(player2.getName());

        // New table obj.
        table = new Table(player1, player2, this, team1Rounds, team1TotalPoints, team2Rounds, team2TotalPoints, Constants.SINGLE_PLAYER_MODE, buttonTruco, buttonDecline);

        // Let's start the game with the real player..
        table.setNextPlayer(player1.getTeam());

        startNewRound(1);
    }

    private void startNewRound(int currentPlayerNumber) {
        Log.d("StartActivity", "Starting new round");

        table.startGame();

        //table.debugGame();

        // Units for the image size
        float factor = getFactor();
        Bitmap cardMatrix = getCardMatrix();

        // Set the imageViews and reset any animation from previous rounds..
        resetImageViews(factor);

        // GenericBitmap for opponents cards..
        Bitmap genericCardBitmap = Card.getGenericCardBitmap(cardMatrix, factor);

        // Middle cards
        Card turnedCard = table.getTurnedCard();
        turnedCard.setCardImage(turnedCardImageView);
        turnedCard.getCardImage().setImageBitmap(turnedCard.getCardBitmap(cardMatrix, factor, 0.6f));
        Card genericCard = table.getTurnedCard();
        genericCard.setCardImage(genericCardImageView);
        genericCard.getCardImage().setImageBitmap(Card.getGenericCardBitmap(cardMatrix, factor, 0.6f));
        genericCard.getCardImage().setTranslationX(40f);

        // Players
        Player playerOne = null;
        Player playerTwo = null;

        if (currentPlayerNumber == 1) {
            playerTwo = table.getTeam2().getPlayers().get(0);
            playerOne = table.getTeam1().getPlayers().get(0);
        }
        else if (currentPlayerNumber == 2) {
            playerOne = table.getTeam2().getPlayers().get(0);
            playerTwo = table.getTeam1().getPlayers().get(0);
        }

        Card card1 = playerTwo.getCards().get(0);
        card1.setCardImage(card1Player2);
        card1.getCardImage().setImageBitmap(/*card1.getCardBitmap(icon, factor)*/ genericCardBitmap);
        card1.setRealImageCard(card1.getCardBitmap(cardMatrix, factor));
        //card1.setCardAnimation(table,playerCPU);

        Card card2 = playerTwo.getCards().get(1);
        card2.setCardImage(card2Player2);
        card2.getCardImage().setImageBitmap(/*card2.getCardBitmap(icon, factor)*/  genericCardBitmap);
        card2.setRealImageCard(card2.getCardBitmap(cardMatrix, factor));
        //card2.setCardAnimation(table, playerCPU);

        Card card3 = playerTwo.getCards().get(2);
        card3.setCardImage(card3Player2);
        card3.getCardImage().setImageBitmap(/*card3.getCardBitmap(icon, factor)*/  genericCardBitmap);
        card3.setRealImageCard(card3.getCardBitmap(cardMatrix, factor));
        //card3.setCardAnimation(table,playerCPU);

        Card card4 = playerOne.getCards().get(0);
        card4.setCardImage(card1Player1);
        card4.getCardImage().setImageBitmap(card4.getCardBitmap(cardMatrix, factor));
        card4.setCardAnimation(table,playerOne);

        Card card5 = playerOne.getCards().get(1);
        card5.setCardImage(card2Player1);
        card5.getCardImage().setImageBitmap(card5.getCardBitmap(cardMatrix, factor));
        card5.setCardAnimation(table, playerOne);

        Card card6 = playerOne.getCards().get(2);
        card6.setCardImage(card3Player1);
        card6.getCardImage().setImageBitmap(card6.getCardBitmap(cardMatrix, factor));
        card6.setCardAnimation(table,playerOne);
    }

    private void sendClientCardsOfRound(boolean newGame) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.JSON_PLAYER1, player1.toJsonObject());
            jsonObject.put(Constants.JSON_PLAYER2, player2.toJsonObject());
            jsonObject.put(Constants.JSON_CARD_TURNED, table.getTurnedCard().toJsonObject());

            if (newGame)
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_NEW_GAME);
            else
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_NEW_TURN);

            jsonObject.put(Constants.JSON_SCORE_TEAM1, table.team1TotalPoints.getText());
            jsonObject.put(Constants.JSON_SCORE_TEAM2, table.team2TotalPoints.getText());
            jsonObject.put(Constants.JSON_SCORE_ROUND_TEAM1, table.team1Rounds.getText());
            jsonObject.put(Constants.JSON_SCORE_ROUND_TEAM2, table.team2Rounds.getText());

            BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
            SocketOutputHandler.send();


        } catch (Exception e) {
            Log.e("Error sending jsonMonster", e.getMessage());
        }
    }

    private Bitmap getCardMatrix() {
        return BitmapFactory.decodeResource(StartActivity.this.getResources(), R.drawable.cards);
    }

    public float getFactor() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        Log.d("DisplayMetrics for this device", displayMetrics.toString());
        return displayMetrics.density / 2;
    }

    /**
     * Instance of BroadcastReceiver that will implement the method onReceive. This method will be called when a new building gets inserted in the listBuildings
     * */
    private BroadcastReceiver newRoundCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Create a separate thread to execute the action of CPU, just to let the animation execute fluently..
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (table.isGameFinished()) {
                        String winner = (table.getTeam1().getPoints() > table.getTeam2().getPoints() ? table.getTeam1().getPlayers().get(0).getName() : table.getTeam2().getPlayers().get(0).getName());

                        Log.d("Winner of the game!!!!!!!!!! ", "The winner is " + winner);
                        Toast.makeText(StartActivity.this, "The winner is " + winner, Toast.LENGTH_LONG).show();
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_NOTIFY_WINNER_TO_CLIENT);
                            jsonObject.put(Constants.JSON_SCORE_TEAM1, table.team1TotalPoints.getText());
                            jsonObject.put(Constants.JSON_SCORE_TEAM2, table.team2TotalPoints.getText());
                            jsonObject.put(Constants.JSON_SCORE_ROUND_TEAM1, table.team1Rounds.getText());
                            jsonObject.put(Constants.JSON_SCORE_ROUND_TEAM2, table.team2Rounds.getText());
                            jsonObject.put(Constants.JSON_WINNER, winner);
                            BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
                            SocketOutputHandler.send();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    else {
                        Log.d("New Round BroadcastReceiver", "starting new round...");
                        startNewRound(1);
                        // If I'm a server, I'll notify the client that a new round is starting, with new cards..
                        if (gameMode.equals(Constants.MULTI_PLAYER_MODE_SERVER)) {
                            sendClientCardsOfRound(false);

                            // If the next player is the client, let's notify him.
                            if (table.getNextPlayer() == 2) {
                                table.notifyToPlay();
                            }

                        }
                        // If it's not a server (in this case it will be a client mode), I'll notify the AI if necessary.
                        else {
                            if (table.getNextPlayer() == 2) {
                                LocalBroadcastManager.getInstance(StartActivity.this).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_PLAYER));
                            }
                        }
                    }
                }
            }, Constants.DELAY_CPU);
        }
    };


    /**
     * Instance of BroadcastReceiver that will implement the method onReceive. This method will be called when its the time of the CPU to play.
     * */
    private BroadcastReceiver cpuCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        // Create a separate thread to execute the action of CPU, just to let the animation execute fluently..
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Log.d("CPU BroadcastReceiver", "playing ...");
                int randomCard = (int) Math.random() % player2.getCards().size();
                // todo place to put my dreamed AI
                Card card = player2.getCards().get(randomCard);
                card.getCardImage().setImageBitmap(card.getRealImageCard());
                card.cardAction(table, player2, false);


            }
        }, Constants.DELAY_CPU);
        }
    };


    /**
     * Instance of BroadcastReceiver that will implement the method onReceive. This method will be called when its the time of the CPU to play.
     * */
    private BroadcastReceiver cpuAnswerTruco = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Create a separate thread to execute the action of CPU, just to let the animation execute fluently..
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Log.d("CPU BroadcastReceiver", "answering truco ...");

                    boolean hasGoodCard = false;
                    for (Card card : player2.getCards()) {
                        if (card.getValue() >= 7) {
                            hasGoodCard = true;
                        }
                        else if (GameRules.isManilha(table.getTurnedCard(), card)) {
                            hasGoodCard = true;
                        }
                    }

                    if (hasGoodCard) {
                        table.clientAcceptTruco();
                    }
                    else {
                        table.trucoDeclined(player1.getTeam());
                    }


                }
            }, Constants.DELAY_CPU);
        }
    };


    /**
     * Instance of BroadcastReceiver that will implement the method onReceive. This method will be called when some packet arrive by the bluetooth socket.
     * */
    private BroadcastReceiver socketInput = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(new String(BlueTrucoSocket.bufferInput.remove(0)));
            } catch (JSONException e) {
                Log.e("Error getting byte[] from buffer", e.getMessage());
            }

            switch (StartActivity.this.gameMode) {
                case Constants.MULTI_PLAYER_MODE_CLIENT:

                    try {
                        Log.d("BroadcastReceiver - socketInput - CLIENT", "Received package: " + jsonObject);

                        TextView team1Rounds = (TextView) findViewById(R.id.team1RoundsTextView);
                        TextView team2Rounds = (TextView) findViewById(R.id.team2RoundsTextView);
                        TextView team1TotalPoints = (TextView) findViewById(R.id.team1PointsTextView);
                        TextView team2TotalPoints = (TextView) findViewById(R.id.team2PointsTextView);

                        switch (jsonObject.get(Constants.SOCKET_PACKAGE_TYPE).toString()) {

                            case Constants.SOCKET_SERVER_NEW_GAME: // New game starting!!
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "New Game! Cards set: " + jsonObject.toString());
                                startMultiPlayerClient(jsonObject);
                            break;
                            case Constants.SOCKET_SERVER_NEW_TURN: // New turn starting!!
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "Cards set: " + jsonObject.toString());
                                startMultiPlayerClient(jsonObject);
                            break;
                            case Constants.SOCKET_SERVER_NOTIFY_WINNER_TO_CLIENT: // New turn starting!!
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "There is a Winner: " + jsonObject.toString());
                                Toast.makeText(StartActivity.this, "The winner is " + jsonObject.get(Constants.JSON_WINNER), Toast.LENGTH_LONG).show();


                                team1Rounds.setText(jsonObject.get(Constants.JSON_SCORE_ROUND_TEAM1).toString());
                                team2Rounds.setText(jsonObject.get(Constants.JSON_SCORE_ROUND_TEAM2).toString());
                                team1TotalPoints.setText(jsonObject.get(Constants.JSON_SCORE_TEAM1).toString());
                                team2TotalPoints.setText(jsonObject.get(Constants.JSON_SCORE_TEAM2).toString());

                            break;
                            case Constants.SOCKET_SERVER_PLAY_CARD: // New card throw by server!!
                                Card serverCardPlayed = new Card().fromJsonObject((JSONObject)jsonObject.get(Constants.JSON_CARD_PLAYED));
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "Server throws an " + serverCardPlayed.toString());
                                for (Card card : player1.getCards()) {
                                    if (card.equals(serverCardPlayed)) {
                                        card.cardAction(table, player1, true);
                                        break;
                                    }
                                }
                            break;
                            case Constants.SOCKET_SERVER_DECLINE_TRUCO:
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "Server declines truco");
                                Toast.makeText(StartActivity.this, player1.getName() + " declined truco", Toast.LENGTH_LONG).show();
                            break;
                            case Constants.SOCKET_SERVER_ACCEPT_TRUCO:
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "Client accepts truco");
                                Toast.makeText(StartActivity.this, player1.getName() + " accepted truco", Toast.LENGTH_LONG).show();
                                table.setNextPlayer(table.getNextPlayerAux());
                                table.hideTrucoButton();
                            break;
                            case Constants.SOCKET_SERVER_ASK_TRUCO:
                                Log.d("BroadcastReceiver - socketInput - CLIENT", "Server asks truco");

                                // lock any card playing while the player don't answer the truco..
                                final int oldNextPlayer = table.getNextPlayer();
                                table.setNextPlayer(0);

                                final int value =  jsonObject.getInt(Constants.JSON_TRUCO_VALUE);

                                Toast.makeText(StartActivity.this,  player1.getName() +" asks "+(value == 3? "truco" : value + "")+"!", Toast.LENGTH_LONG).show();
                                buttonTruco.setVisibility(View.VISIBLE);
                                buttonTruco.setText("Accept");
                                buttonTruco.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_CLIENT_ACCEPT_TRUCO);
                                        } catch (JSONException e) {
                                            Log.e("Error creating JSON obj to accept truco for next player", e.getMessage());
                                        }
                                        // Let's send to the client the card played...
                                        BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
                                        StartActivity.SocketOutputHandler.send();

                                        // unlock any card playing
                                        table.setNextPlayer(oldNextPlayer);

                                        if (value == 12) {
                                            table.hideTrucoButton();
                                        }
                                        else {
                                            table.setListenerButtonTruco(value + 3);
                                        }
                                        table.hideDeclineButton();
                                    }
                                });


                                buttonDecline.setVisibility(View.VISIBLE);
                                buttonDecline.setText("Decline");
                                buttonDecline.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_CLIENT_DECLINE_TRUCO);
                                        } catch (JSONException e) {
                                            Log.e("Error creating JSON obj to decline truco for next player", e.getMessage());
                                        }
                                        // Let's send to the client the card played...
                                        BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
                                        StartActivity.SocketOutputHandler.send();

                                        // unlock any card playing
                                        //table.setNextPlayer(oldNextPlayer);
                                        table.setListenerButtonTruco(3);
                                        table.hideDeclineButton(); // todo maybe is not the best place to put it...
                                    }
                                });

                            break;
                            case Constants.SOCKET_SERVER_NOTIFY_CLIENT_TO_PLAY:
                                Log.d("BroadcastReceiver - socketInput - CLIENT",  "Time to play");

                                team1Rounds.setText(jsonObject.get(Constants.JSON_SCORE_ROUND_TEAM1).toString());
                                team2Rounds.setText(jsonObject.get(Constants.JSON_SCORE_ROUND_TEAM2).toString());
                                team1TotalPoints.setText(jsonObject.get(Constants.JSON_SCORE_TEAM1).toString());
                                team2TotalPoints.setText(jsonObject.get(Constants.JSON_SCORE_TEAM2).toString());

                                Toast.makeText(StartActivity.this, "Time to play", Toast.LENGTH_LONG).show();
                                table.setNextPlayer(player2.getTeam());
                            break;
                        }
                    } catch (Exception e) {
                        Log.d("BroadcastReceiver - socketInput", e.getMessage());
                    }
                break;
                case Constants.MULTI_PLAYER_MODE_SERVER:
                    // todo some errors occurred here, pay attention!

                    try {
                        Log.d("BroadcastReceiver - socketInput - SERVER", "Received package: " + jsonObject);

                        switch (jsonObject.get(Constants.SOCKET_PACKAGE_TYPE).toString()) {

                            case Constants.SOCKET_CLIENT_HANDSHAKE: // Handshake...!!
                                player2 = new Player(jsonObject.getString(Constants.JSON_PLAYER_NAME),1);
                                Log.d("BroadcastReceiver - socketInput - SERVER",  "Set player2: " + player2.getName());
                                startMultiPlayerServer();
                            break;
                            case Constants.SOCKET_CLIENT_PLAY_CARD: // New card throw by client!!
                                Card clientCardPlayed = new Card().fromJsonObject((JSONObject)jsonObject.get(Constants.JSON_CARD_PLAYED));
                                Log.d("BroadcastReceiver - socketInput - SERVER", "Client throws an " + clientCardPlayed.toString());
                                for (Card card : player2.getCards()) {
                                    if (card.equals(clientCardPlayed)) {
                                        card.cardAction(table, player2, true);
                                        break;
                                    }
                                }
                            break;
                            case Constants.SOCKET_CLIENT_NOTIFY_SERVER_TO_PLAY:
                                Log.d("BroadcastReceiver - socketInput - SERVER",  "Time to play");
                                Toast.makeText(StartActivity.this, "Time to play", Toast.LENGTH_LONG).show();
                                table.setNextPlayer(player1.getTeam());
                            break;
                            case Constants.SOCKET_CLIENT_DECLINE_TRUCO:
                                Log.d("BroadcastReceiver - socketInput - SERVER", "Client declines truco");
                                Toast.makeText(StartActivity.this, player2.getName() + " declined truco", Toast.LENGTH_LONG).show();
                                table.trucoDeclined(table.getTeam1());
                            break;
                            case Constants.SOCKET_CLIENT_ACCEPT_TRUCO:
                                Log.d("BroadcastReceiver - socketInput - SERVER", "Client accepts truco");
                                Toast.makeText(StartActivity.this, player2.getName() + " accepted truco", Toast.LENGTH_LONG).show();
                                table.setNextPlayer(table.getNextPlayerAux());
                                table.hideTrucoButton();

                                if (table.getPointsForRound() == 1)
                                    table.setPointsForRound(3);
                                else if (table.getPointsForRound() == 3)
                                    table.setPointsForRound(6);
                                else if (table.getPointsForRound() == 6)
                                    table.setPointsForRound(9);
                                else if (table.getPointsForRound() == 9)
                                    table.setPointsForRound(12);


                                //table.hideDeclineButton();

                            break;
                            case Constants.SOCKET_CLIENT_ASK_TRUCO:
                                Log.d("BroadcastReceiver - socketInput - SERVER", "Client asks truco");

                                // lock any card playing while the player don't answer the truco..
                                final int oldNextPlayer = table.getNextPlayer();
                                table.setNextPlayer(0);

                                final int value =  jsonObject.getInt(Constants.JSON_TRUCO_VALUE);

                                Toast.makeText(StartActivity.this,  player2.getName() +" asks "+(value == 3? "truco" : value + "")+"!", Toast.LENGTH_LONG).show();
                                buttonTruco.setVisibility(View.VISIBLE);
                                buttonTruco.setText("Accept");
                                buttonTruco.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_ACCEPT_TRUCO);
                                        } catch (JSONException e) {
                                            Log.e("Error creating JSON obj to accept truco for next player", e.getMessage());
                                        }
                                        // Let's send to the client the card played...
                                        BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
                                        StartActivity.SocketOutputHandler.send();

                                        table.setPointsForRound(value);
                                        table.hideDeclineButton();

                                        // unlock any card playing
                                        table.setNextPlayer(oldNextPlayer);

                                        if (value == 12) {
                                            table.hideTrucoButton();
                                        }
                                        else {
                                            table.setListenerButtonTruco(value + 3);
                                        }

                                    }
                                });


                                buttonDecline.setVisibility(View.VISIBLE);
                                buttonDecline.setText("Decline");
                                buttonDecline.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_DECLINE_TRUCO);
                                        } catch (JSONException e) {
                                            Log.e("Error creating JSON obj to decline truco for next player", e.getMessage());
                                        }
                                        // Let's send to the client the card played...
                                        BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
                                        StartActivity.SocketOutputHandler.send();

                                        table.trucoDeclined(table.getTeam2());
                                        // unlock any card playing
                                        table.setNextPlayer(oldNextPlayer);

                                        table.setListenerButtonTruco(3);
                                        table.hideDeclineButton();
                                    }
                                });

                            break;
                        }

                    } catch (Exception e) {
                        Log.d("ServerSocketInputHandler - onPreExecute", e.getMessage());
                    }


                break;
            }

            //Toast.makeText(StartActivity.this, "Packet:" + new String(BlueTrucoSocket.bufferInput.get(0)), Toast.LENGTH_LONG ).show();
        }
    };

    public class ServerSocketInputHandler extends AsyncTask<String, Integer, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("ServerSocketInputHandler - onPreExecute", "");
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                Log.d("ServerSocketInputHandler - doInBackground", "doInBackground");
                InputStream is = BlueTrucoSocket.socket.getInputStream();

                // Set the method to receive the information that the user already had played. This will call the AI to play.
                LocalBroadcastManager.getInstance(StartActivity.this).registerReceiver(socketInput, new IntentFilter(Constants.INTENT_BROADCAST_SOCKET_INPUT));

                while (BlueTrucoSocket.socket.isConnected()) {
                    byte[] bufferInput = new byte[Constants.BUFFER_SIZE];
                    is.read(bufferInput);
                    BlueTrucoSocket.bufferInput.add(bufferInput);
                    Log.d("ServerSocketInputHandler - doInBackground", "Received Message: " + new String(bufferInput));

                    // Call the handler to deal with the packet.
                    LocalBroadcastManager.getInstance(StartActivity.this).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_SOCKET_INPUT));
                }

            } catch (IOException e) {
                Log.d("ServerSocketInputHandler - doInBackground", e.getMessage());
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d("ServerSocketInputHandler - onPostExecute", "Unregistered LocalBroadcastManager socketInput.");

            // After the connection is closed, we just unregister the broadcastReceiver..
            LocalBroadcastManager.getInstance(StartActivity.this).unregisterReceiver(socketInput);
        }
    }

    public class ClientSocketInputHandler extends AsyncTask<String, Integer, Boolean>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("ClientSocketInputHandler - onPreExecute", "");
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                Log.d("ClientSocketInputHandler", "doInBackground");
                InputStream is = BlueTrucoSocket.socket.getInputStream();

                // Set the method to receive the information that the user already had played. This will call the AI to play.
                LocalBroadcastManager.getInstance(StartActivity.this).registerReceiver(socketInput, new IntentFilter(Constants.INTENT_BROADCAST_SOCKET_INPUT));

                while (BlueTrucoSocket.socket.isConnected()) {
                    byte[] bufferInput = new byte[Constants.BUFFER_SIZE];
                    is.read(bufferInput);
                    BlueTrucoSocket.bufferInput.add(bufferInput);
                    Log.d("ClientSocketInputHandler", "Received Message: " + new String(bufferInput));

                    // Call the handler to deal with the packet.
                    LocalBroadcastManager.getInstance(StartActivity.this).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_SOCKET_INPUT));
                }

            } catch (IOException e) {
                Log.d("ClientSocketInputHandler", e.getMessage());
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d("ClientSocketInputHandler", "onPostExecute");

            // After the connection is closed, we just unregister the broadcastReceiver..
            LocalBroadcastManager.getInstance(StartActivity.this).unregisterReceiver(socketInput);
        }
    }

    public static class SocketOutputHandler {
        public static boolean send() {
            Handler handler = new Handler();
            return handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        OutputStream outputStream = BlueTrucoSocket.socket.getOutputStream();
                        while (!BlueTrucoSocket.bufferOutput.isEmpty()) {
                            byte[] toBeWrite = BlueTrucoSocket.bufferOutput.remove(0);
                            outputStream.write(toBeWrite);
                            Log.d("SocketOutputHandler", "Message sent: " + new String(toBeWrite));
                        }

                    } catch (Exception e) {
                        Log.d("SocketOutputHandler", e.getMessage());
                    }
                }
            });
        }
    }


    /**
     * Method that will clean any animation in the ImageViews that display the cards on the game.
     * */
    private void resetImageViews(float factor) {
        if (turnedCardImageView == null) turnedCardImageView = (ImageView) findViewById(R.id.imageView7);
        if (genericCardImageView == null) genericCardImageView = (ImageView) findViewById(R.id.imageView8);
        if (card1Player2 == null) card1Player2 = (ImageView) findViewById(R.id.imageView);
        if (card2Player2 == null) card2Player2 = (ImageView) findViewById(R.id.imageView2);
        if (card3Player2 == null) card3Player2 = (ImageView) findViewById(R.id.imageView3);
        if (card1Player1 == null) card1Player1 = (ImageView) findViewById(R.id.imageView4);
        if (card2Player1 == null) card2Player1 = (ImageView) findViewById(R.id.imageView5);
        if (card3Player1 == null) card3Player1 = (ImageView) findViewById(R.id.imageView6);


        card1Player2.setImageBitmap(null);
        card1Player2.setAnimation(null);
        card1Player2.setOnClickListener(null);
        card1Player2.setVisibility(View.VISIBLE);
        card1Player2.setScaleX(1f);
        card1Player2.setScaleY(1f);

        if (factor == 0.75) {
            int maxWidth = 120;
            int maxHeight = 186;

            card1Player2.setAdjustViewBounds(true);
            card1Player2.setMaxWidth(maxWidth);
            card1Player2.setMaxHeight(maxHeight);

            card2Player2.setAdjustViewBounds(true);
            card2Player2.setMaxWidth(maxWidth);
            card2Player2.setMaxHeight(maxHeight);

            card3Player2.setAdjustViewBounds(true);
            card3Player2.setMaxWidth(maxWidth);
            card3Player2.setMaxHeight(maxHeight);


            card1Player1.setAdjustViewBounds(true);
            card1Player1.setMaxWidth(maxWidth);
            card1Player1.setMaxHeight(maxHeight);

            card2Player1.setAdjustViewBounds(true);
            card2Player1.setMaxWidth(maxWidth);
            card2Player1.setMaxHeight(maxHeight);

            card3Player1.setAdjustViewBounds(true);
            card3Player1.setMaxWidth(maxWidth);
            card3Player1.setMaxHeight(maxHeight);

            TextView team1 = (TextView) findViewById(R.id.team1TextView);
            TextView team2 = (TextView) findViewById(R.id.team2TextView);
            TextView team1Rounds = (TextView) findViewById(R.id.team1RoundsTextView);
            TextView team2Rounds = (TextView) findViewById(R.id.team2RoundsTextView);
            TextView team1TotalPoints = (TextView) findViewById(R.id.team1PointsTextView);
            TextView team2TotalPoints = (TextView) findViewById(R.id.team2PointsTextView);

            TextView players = (TextView) findViewById(R.id.textViewTeams);
            TextView roundsWon = (TextView) findViewById(R.id.textViewRoundsWon);
            TextView totalPoints = (TextView) findViewById(R.id.textViewTotalPoints);

            team1.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            team2.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            team1Rounds.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            team2Rounds.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            team1TotalPoints.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            team2TotalPoints.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);

            players.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            roundsWon.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);
            totalPoints.setTextAppearance(this, R.style.TextAppearance_AppCompat_Medium);

        }



        card2Player2.setImageBitmap(null);
        card2Player2.setAnimation(null);
        card2Player2.setOnClickListener(null);
        card2Player2.setVisibility(View.VISIBLE);
        card2Player2.setScaleX(1f);
        card2Player2.setScaleY(1f);

        card3Player2.setImageBitmap(null);
        card3Player2.setAnimation(null);
        card3Player2.setOnClickListener(null);
        card3Player2.setVisibility(View.VISIBLE);
        card3Player2.setScaleX(1f);
        card3Player2.setScaleY(1f);

        card1Player1.setImageBitmap(null);
        card1Player1.setAnimation(null);
        card1Player1.setOnClickListener(null);
        card1Player1.setVisibility(View.VISIBLE);
        card1Player1.setScaleX(1f);
        card1Player1.setScaleY(1f);

        card2Player1.setImageBitmap(null);
        card2Player1.setAnimation(null);
        card2Player1.setOnClickListener(null);
        card2Player1.setVisibility(View.VISIBLE);
        card2Player1.setScaleX(1f);
        card2Player1.setScaleY(1f);

        card3Player1.setImageBitmap(null);
        card3Player1.setAnimation(null);
        card3Player1.setOnClickListener(null);
        card3Player1.setVisibility(View.VISIBLE);
        card3Player1.setScaleX(1f);
        card3Player1.setScaleY(1f);
    }
}
