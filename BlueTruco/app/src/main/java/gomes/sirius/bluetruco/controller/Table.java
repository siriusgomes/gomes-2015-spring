package gomes.sirius.bluetruco.controller;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import gomes.sirius.bluetruco.activities.StartActivity;
import gomes.sirius.bluetruco.constants.Constants;
import gomes.sirius.bluetruco.model.Card;
import gomes.sirius.bluetruco.model.CardSuit;
import gomes.sirius.bluetruco.model.CardValue;
import gomes.sirius.bluetruco.model.Player;
import gomes.sirius.bluetruco.model.Round;
import gomes.sirius.bluetruco.model.Team;

/**
 * Created by Sírius on 3/31/2015.
 */
public class Table {

    private List<Card> listCards = new ArrayList<Card>();
    private Team team1;
    private Team team2;
    private Card turnedCard;
    private StartActivity context;
    private String gameMode;

    private int nextPlayer;
    private int nextPlayerAux;
    private int pointsForRound = 1;

    public TextView team1Rounds;
    public TextView team2Rounds;
    public TextView team1TotalPoints;
    public TextView team2TotalPoints;
    private Button buttonTruco;
    private Button buttonDecline;


    public void setTable(List<Player> playersTeam1, List<Player> playersTeam2, StartActivity context, TextView team1Rounds, TextView team1TotalPoints, TextView team2Rounds, TextView team2TotalPoints, String gameMode, Button buttonTruco, Button buttonDecline) {
        team1 = new Team(1,playersTeam1);
        team2 = new Team(2,playersTeam2);
        this.context = context;
        this.team1Rounds = team1Rounds;
        this.team1TotalPoints = team1TotalPoints;
        this.team2Rounds = team2Rounds;
        this.team2TotalPoints = team2TotalPoints;
        this.gameMode = gameMode;
        this.buttonTruco = buttonTruco;
        this.buttonDecline = buttonDecline;

        setListenerButtonTruco(3);

    }

    public Table(List<Player> playersTeam1, List<Player> playersTeam2, StartActivity context, TextView team1Rounds, TextView team1TotalPoints, TextView team2Rounds, TextView team2TotalPoints, String gameMode, Button buttonTruco, Button buttonDecline) {
        setTable(playersTeam1, playersTeam2, context, team1Rounds, team1TotalPoints, team2Rounds, team2TotalPoints, gameMode, buttonTruco, buttonDecline);
    }

    public Table(Player playerTeam1, Player playerTeam2, StartActivity context, TextView team1Rounds, TextView team1TotalPoints, TextView team2Rounds, TextView team2TotalPoints, String gameMode, Button buttonTruco, Button buttonDecline) {
       List<Player> listPlayersTeam1 = new ArrayList<Player>();
       List<Player> listPlayersTeam2 = new ArrayList<Player>();

       listPlayersTeam1.add(playerTeam1);
       listPlayersTeam2.add(playerTeam2);

       setTable(listPlayersTeam1, listPlayersTeam2, context, team1Rounds, team1TotalPoints, team2Rounds, team2TotalPoints, gameMode, buttonTruco, buttonDecline);

    }

    /**
     * This method will start the game:
     * 1-Shuffling the cards
     * 2-Giving the cards for the players in both teams
     * 3-Turning one card*/
       //todo some animations here would be nice...
     public void startGame() {
        Log.d("Table", "Starting new Game");

        GameRules.turnsWinner = new int[3];
        GameRules.turnsWinner[0] = -1;
        GameRules.turnsWinner[1] = -1;
        GameRules.turnsWinner[2] = -1;
        GameRules.listRound = new ArrayList<Round>();

        if (gameMode.equals(Constants.SINGLE_PLAYER_MODE) || gameMode.equals(Constants.MULTI_PLAYER_MODE_SERVER)) {
            listCards.clear();
            this.shuffleCards();
            Log.d("Table", "Cards shuffled");

            for (Player player : team1.getPlayers()) {
                player.setCards(this.get3Cards());
                player.debugPlayer();
            }
            for (Player player : team2.getPlayers()) {
                player.setCards(this.get3Cards());
                player.debugPlayer();
            }

            this.turnedCard = listCards.remove((int) Math.random() % listCards.size());
            Log.d("Table", "Card turned is: " + this.turnedCard);
            setScoreOnScreen();
        }
    }

    /**
     * This method will start the game by receiving the cards from the server.
     * */
  /*  public void startGameRemotePlayer() {
        Log.d("Table", "Starting new Game");

        GameRules.turnsWinner = new int[3];
        GameRules.turnsWinner[0] = -1;
        GameRules.turnsWinner[1] = -1;
        GameRules.turnsWinner[2] = -1;
        listRound = new ArrayList<Round>();

        setScoreOnScreen();
    }
*/

    public void hideTrucoButton() {
        this.buttonTruco.setVisibility(View.INVISIBLE);
        this.buttonTruco.setOnClickListener(null);
    }
    public void hideDeclineButton() {
        this.buttonDecline.setVisibility(View.INVISIBLE);
        this.buttonDecline.setOnClickListener(null);
    }

    public void clientAcceptTruco() {
        Toast.makeText(context, "CPU accepted truco", Toast.LENGTH_LONG).show();
        if (getPointsForRound() == 1)
            setPointsForRound(3);
        else if (getPointsForRound() == 3)
            setPointsForRound(6);
        else if (getPointsForRound() == 6)
            setPointsForRound(9);
        else if (getPointsForRound() == 9)
            setPointsForRound(12);
    }

    public void setListenerButtonTruco(final int value) {

        this.buttonTruco.setVisibility(View.VISIBLE);
        if (value == 3) {
            this.buttonTruco.setText("Truco");
        }
        else {
            this.buttonTruco.setText("Ask " + value);
        }

        this.buttonTruco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("buttonTruco", "Someone asked truco!");
                if (!gameMode.equals(Constants.SINGLE_PLAYER_MODE)) {
                    if (gameMode.equals(Constants.MULTI_PLAYER_MODE_SERVER) && getNextPlayer() == 1 || gameMode.equals(Constants.MULTI_PLAYER_MODE_CLIENT) && getNextPlayer() == 2) {
                        sendTruco(value);
                        nextPlayerAux = nextPlayer;
                        nextPlayer = 0;
                        Toast.makeText(context, "Waiting answer from opponent", Toast.LENGTH_LONG).show();
                        Table.this.buttonTruco.setVisibility(View.INVISIBLE);
                    } else {
                        Toast.makeText(context, "You have to wait your time to play!", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    if (getNextPlayer() == 1) {
                        Log.d("buttonTruco", "The player 1 asked truco for the CPU!");
                        // Notify the AI...
                        LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_ANSWER_TRUCO));

                        hideTrucoButton();
                    }
                    else if (getNextPlayer() == 2) {
                        Log.d("buttonTruco", "The CPU asked truco for the player 1!");

                        // lock any card playing while the player don't answer the truco..
                        final int oldNextPlayer = getNextPlayer();
                        setNextPlayer(0);


                        Toast.makeText(context, "CPU asks "+(value == 3? "truco" : value + "")+"!", Toast.LENGTH_LONG).show();
                        buttonTruco.setVisibility(View.VISIBLE);
                        buttonTruco.setText("Accept");
                        buttonTruco.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setPointsForRound(value);
                                hideDeclineButton();

                                // unlock any card playing
                                setNextPlayer(oldNextPlayer);

                                if (value == 12) {
                                    hideTrucoButton();
                                }
                                else {
                                    setListenerButtonTruco(value + 3);
                                }

                                // Notify the AI...
                                LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_PLAYER));
                            }
                        });


                        buttonDecline.setVisibility(View.VISIBLE);
                        buttonDecline.setText("Decline");
                        buttonDecline.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                trucoDeclined(getTeam2());
                                // unlock any card playing
                                setNextPlayer(oldNextPlayer);

                                setListenerButtonTruco(3);
                                hideDeclineButton();
                            }
                        });
                    }
                }
            }
        });
    }

    public void trucoDeclined(Team turnWinner) {
        if (gameMode.equals(Constants.SINGLE_PLAYER_MODE) && turnWinner.getTeamNumber() == 1) {
            Toast.makeText(context, "CPU declined truco", Toast.LENGTH_LONG).show();
        }

        if (turnWinner != null) {
            turnWinner.addPoints(pointsForRound);

            // We set the next player based in the turnWinner.
            setNextPlayer(turnWinner);

            // If the winner of the turn is the BLUETOOTH GUY... let's notify him tos start the next turn!
            if (turnWinner.getTeamNumber() == 2) {
                setNextPlayer(team2);
                // Notify the client to play again..
                //SnotifyToPlay();
            }
            else {
                setNextPlayer(team1);
            }

            // Update the score in the screen.
            setScoreOnScreen();

            // Starts a new round by sending the new cards to the client.. and If the winner of the turn is the BLUETOOTH GUY... it'll notify him tos start the next turn!
            LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_ROUND));

            this.setPointsForRound(1);
            this.setListenerButtonTruco(3);
            this.hideDeclineButton();
        }
    }

    /**
     * Function that will receive one play from one player.
     * */
    public void playRound(Player player, Card card) {


        switch (gameMode) {
            case Constants.SINGLE_PLAYER_MODE:
                Log.d("playRound", Constants.SINGLE_PLAYER_MODE);
                GameRules.listRound.add(new Round(player,card));
                Log.d("Player " + player.toString() + " throws an ", card.toString());

                if (numberOfPlayers() == GameRules.listRound.size()) {

                    // Sets the roundWinner for the round and clean all the cards in the screen.
                    Round roundWinner = GameRules.getWinnerAndCleanAnimation(getTurnedCard());

                    // Set the roundWinner of the round based in the roundWinner.
                    GameRules.setTurnWinner(roundWinner);

                    // Update the score in the screen.
                    setScoreOnScreen();

                    // Clean the listRound list.
                    GameRules.listRound.clear();

                    // Get the possible turnWinner team, or null if there is no roundWinner yet.
                    Team turnWinner = getTeamByNumber(GameRules.verifyTurnWinner());

                    // If there is a turnWinner, let's set the score, set the next player and end the function
                    if (turnWinner != null) {
                        turnWinner.addPoints(getPointsForRound()); // todo truco possibility...

                        // We set the next player based in the turnWinner.
                        setNextPlayer(turnWinner);

                        // Update the score in the screen.
                        setScoreOnScreen();

                        LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_ROUND));

                        this.setPointsForRound(1);
                        this.setListenerButtonTruco(3);
                        this.hideDeclineButton();
                    }
                    // There is no roundWinner of the turn yet, this is, we're in the 1st or 2nd round yet, so let's check who is the roundWinner of the round to call the AI if necessary.
                    else {
                        // If there is no roundWinner for this round, we have a TIE!
                        if (roundWinner == null) {
                            Log.d("Table", "TIE!!");

                            // If the AI was the last one to throw a card this TIE round, let's notify it to play again.
                            if (player.getPlayerType() == Constants.PLAYER_TYPE_CPU) {
                                setNextPlayer(2);
                                LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_PLAYER));
                            }
                            else {
                                setNextPlayer(1);
                            }
                        }
                        // There is a roundWinner in this round!
                        else {
                            Log.d("Table", "Winner of the turn is: "+ roundWinner.getPlayer().getName() + " from team " + roundWinner.getPlayer().getTeam().getTeamNumber());

                            // If the AI is the roundWinner of the round, let's notify it to play again.
                            if (roundWinner.getPlayer().getPlayerType() == Constants.PLAYER_TYPE_CPU) {
                                setNextPlayer(2);
                                LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_PLAYER));
                            }
                            else {
                                setNextPlayer(1);
                            }
                        }
                    }
                }
                // Just 1 player threw the card, so we'll not check if there is a winner of the round, but we have to check if it's time to AI to play.
                else {

                    // If the last player to throw a card was the REAL player, let's notify the AI to play.
                    if (player.getPlayerType() == Constants.PLAYER_TYPE_REAL) {
                        setNextPlayer(2);
                        LocalBroadcastManager.getInstance(this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_PLAYER));
                    }
                    else {
                        setNextPlayer(1);
                    }
                }
            break;
            case Constants.MULTI_PLAYER_MODE_SERVER:
                Log.d("playRound", Constants.MULTI_PLAYER_MODE_SERVER);
                GameRules.listRound.add(new Round(player,card));
                Log.d("Player " + player.toString() + " throws an ", card.toString());

                // If the guy playing this card is the server..
                if (player.getTeam().getTeamNumber() == 1) {
                    sendCardPlayed(card);
                }

                if (numberOfPlayers() == GameRules.listRound.size()) {

                    // Sets the roundWinner for the round and clean all the cards in the screen.
                    Round roundWinner = GameRules.getWinnerAndCleanAnimation(getTurnedCard());

                    // Set the roundWinner of the round based in the roundWinner.
                    GameRules.setTurnWinner(roundWinner);

                    // Update the score in the screen.
                    setScoreOnScreen();

                    // Clean the listRound list.
                    GameRules.listRound.clear();

                    // Get the possible turnWinner team, or null if there is no roundWinner yet.
                    Team turnWinner = getTeamByNumber(GameRules.verifyTurnWinner());

                    // If there is a turnWinner, let's set the score, set the next player and end the function
                    if (turnWinner != null) {
                        turnWinner.addPoints(pointsForRound);

                        // We set the next player based in the turnWinner.
                        setNextPlayer(turnWinner);

                        // If the winner of the turn is the BLUETOOTH GUY... let's notify him tos start the next turn!
                        if (turnWinner.getTeamNumber() == 2) {
                            setNextPlayer(team2);
                            // Notify the client to play again..
                            //SnotifyToPlay();
                        }
                        else {
                            setNextPlayer(team1);
                        }

                        // Update the score in the screen.
                        setScoreOnScreen();

                        // Starts a new round by sending the new cards to the client.. and If the winner of the turn is the BLUETOOTH GUY... it'll notify him tos start the next turn!
                        LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_ROUND));

                        this.setPointsForRound(1);
                        this.setListenerButtonTruco(3);
                        this.hideDeclineButton();
                    }
                    // There is no roundWinner of the turn yet, this is, we're in the 1st or 2nd round yet, so let's check who is the roundWinner of the round to call the notify the bluetooth guy if necessary.
                    else {
                        // If there is no roundWinner for this round, we have a TIE!
                        if (roundWinner == null) {
                            Log.d("Table", "TIE!!");

                            // If the BLUETOOTH GUY was the last one to throw a card this TIE round, let's notify him to play again.
                            if (player.getTeam().getTeamNumber() == 2) {

                                // We set the next player as the client again.
                                setNextPlayer(player.getTeam());

                                // Notify the client to play again..
                                notifyToPlay();
                            }
                            else {
                                // We set the next player as the server again.
                                setNextPlayer(player.getTeam());
                            }
                        }
                        // There is a roundWinner in this round!
                        else {
                            Log.d("Table", "Winner of the turn is: " + roundWinner.getPlayer().getName() + " from team " + roundWinner.getPlayer().getTeam().getTeamNumber());

                            // If the BLUETOOTH GUY is the roundWinner of the round, let's notify him to play again.
                            if (roundWinner.getPlayer().getTeam().getTeamNumber() == 2) {
                                //LocalBroadcastManager.getInstance(Table.this.context).sendBroadcast(new Intent(Constants.INTENT_BROADCAST_NEXT_PLAYER));
                                notifyToPlay();
                            }
                            else {
                                // We set the next player as the server again.
                                setNextPlayer(roundWinner.getPlayer().getTeam());
                            }
                        }
                    }
                }
                // Just 1 player threw the card, so we'll not check if there is a winner of the round, but we have to check if it's time to AI to play.
                else {

                    // If the last player to throw a card was the local player, let's notify the BLUETOOTH GUY to play.
                    if (player.getTeam().getTeamNumber() == 1) {

                        // We set the next player as the client, and notify him to play..
                        setNextPlayer(team2);
                        notifyToPlay();
                    }
                    // If the last player to throw a card was the BLUETOOTH GUY, let's notify the local player to play.
                    else { // (player.getTeam().getTeamNumber() == 1) {

                        // We set the next player as the client, and notify him to play..
                        setNextPlayer(team1);
                    }
                }
            break;
            case Constants.MULTI_PLAYER_MODE_CLIENT:
                Log.d("playRound", Constants.MULTI_PLAYER_MODE_CLIENT);

                GameRules.listRound.add(new Round(player,card));

                // If the guy playing this card is the client..
                if (player.getTeam().getTeamNumber() == 2) {
                    sendCardPlayed(card);
                    //notifyToPlay();
                    setNextPlayer(team1);
                }

                if (numberOfPlayers() == GameRules.listRound.size()) {

                    // Let's just clean all the cards in the screen.
                    GameRules.getWinnerAndCleanAnimation(getTurnedCard());


                    // Clean the listRound list.
                    GameRules.listRound.clear();
                }

            break;
        }



    }

    public TranslateAnimation getTranslateAnimationForCard(Card cardToTranslate) {
        int from_x = 0;
        int to_x = 0;
        int from_y = 0;
        int to_y = 0;

        int distanceBetweenCards = 0;

        if (context.getFactor() <= 0.75) {
            distanceBetweenCards = Constants.distanceBetweenCardsThrowedMDPI;
        }
        else if (context.getFactor() == 1) {
            distanceBetweenCards = Constants.distanceBetweenCardsThrowedXHDPI;
        }
        else if (context.getFactor() > 1) {
            distanceBetweenCards = Constants.distanceBetweenCardsThrowedXXHDPI;
        }

        to_x = Constants.xTranslateAnimation - (int) cardToTranslate.getCardImage().getX();
        to_y = (int) turnedCard.getCardImage().getY();

       // to_y = cardToTranslate.getCardImage().getY()

        for (Card card : team1.getPlayers().get(0).getCards()) {
            if (cardToTranslate.equals(card)) {
                if (gameMode.equals(Constants.MULTI_PLAYER_MODE_SERVER)) {
                    to_y = -to_y;
                    to_y += distanceBetweenCards;
                }
                else if (gameMode.equals(Constants.MULTI_PLAYER_MODE_CLIENT)) {
                    to_y -= distanceBetweenCards;
                }
                else if (gameMode.equals(Constants.SINGLE_PLAYER_MODE)) {
                    to_y = -to_y;
                    to_y += distanceBetweenCards;
                }
                break;
            }
        }
        for (Card card : team2.getPlayers().get(0).getCards()) {
            if (cardToTranslate.equals(card)) {
                if (gameMode.equals(Constants.MULTI_PLAYER_MODE_SERVER)) {
                    to_y -=distanceBetweenCards;
                }
                else if (gameMode.equals(Constants.MULTI_PLAYER_MODE_CLIENT)) {
                    to_y = -to_y;
                    to_y += distanceBetweenCards;
                }
                else if (gameMode.equals(Constants.SINGLE_PLAYER_MODE)) {
                    to_y -=distanceBetweenCards;
                }
                break;
            }
        }

        return new TranslateAnimation(from_x,to_x,from_y,to_y);
    }

    /**
     * Send the card @card to the server/client.
     * */
    public void sendTruco(int value) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (gameMode.equals(Constants.MULTI_PLAYER_MODE_CLIENT))
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_CLIENT_ASK_TRUCO);
            else
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_ASK_TRUCO);

            jsonObject.put(Constants.JSON_TRUCO_VALUE, value);
        } catch (JSONException e) {
            Log.e("Error creating JSON obj to ask truco for next player", e.getMessage());
        }
        // Let's send to the client the card played...
        BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
        StartActivity.SocketOutputHandler.send();
    }


    /**
     * Send the card @card to the server/client.
     * */
    private void sendCardPlayed(Card card) {
        // Let's send to the client the card played...
        BlueTrucoSocket.bufferOutput.add(getJSONObjectFromCardPlayed(card).toString().getBytes());
        StartActivity.SocketOutputHandler.send();
    }

    /**
     * Send the card @card to the server/client.
     * */
    public void notifyToPlay() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put(Constants.JSON_SCORE_TEAM1, team1TotalPoints.getText());
                    jsonObject.put(Constants.JSON_SCORE_TEAM2, team2TotalPoints.getText());
                    jsonObject.put(Constants.JSON_SCORE_ROUND_TEAM1, team1Rounds.getText());
                    jsonObject.put(Constants.JSON_SCORE_ROUND_TEAM2, team2Rounds.getText());

                    if (gameMode.equals(Constants.MULTI_PLAYER_MODE_CLIENT))
                        jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_CLIENT_NOTIFY_SERVER_TO_PLAY);
                    else
                        jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_NOTIFY_CLIENT_TO_PLAY);
                } catch (JSONException e) {
                    Log.e("Error creating JSON obj to notify next player", e.getMessage());
                }

                // Let's send to the next player the "notification".
                BlueTrucoSocket.bufferOutput.add(jsonObject.toString().getBytes());
                StartActivity.SocketOutputHandler.send();
            }
        }, Constants.DELAY_NOTIFY);
    }

    /**
     * Prepare a JSONObject to be sent as card played.
     * */
    private JSONObject getJSONObjectFromCardPlayed(Card card) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (gameMode.equals(Constants.MULTI_PLAYER_MODE_CLIENT))
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_CLIENT_PLAY_CARD);
            else
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_SERVER_PLAY_CARD);
            jsonObject.put(Constants.JSON_CARD_PLAYED, card.toJsonObject());
        } catch (JSONException e) {
            Log.e("Error converting card to JSON", e.getMessage());
        }
        return jsonObject;
    }

    private int numberOfPlayers() {
        return team1.getPlayers().size() + team2.getPlayers().size();
    }

    public boolean isGameFinished() {
        return (team1.getPoints() >= Constants.POINTS_TO_WIN || team2.getPoints() >= Constants.POINTS_TO_WIN);
    }

    /**
     * Function that will update the score in the screen.
     * */
    private void setScoreOnScreen() {
        int roundsTeam1 = 0;
        int roundsTeam2 = 0;
        for (int i = 0; i < GameRules.turnsWinner.length; i++) {
            if (GameRules.turnsWinner[i] == 1) {
                roundsTeam1++;
            }
            else if (GameRules.turnsWinner[i] == 2) {
                roundsTeam2++;
            }
        }

        team1Rounds.setText(roundsTeam1 + "");
        team2Rounds.setText(roundsTeam2 + "");

        team1TotalPoints.setText(team1.getPoints() + "");
        team2TotalPoints.setText(team2.getPoints() + "");

    }




    private Team getTeamByNumber(int number) {
        if (number == 1) {
            return team1;
        }
        else if (number == 2) {
            return team2;
        }
        else
            return null;
    }

    /**
     * Method that return three cards from the listCards.
     * */
    private List<Card> get3Cards() {
        List<Card> listCard = new ArrayList<Card>();
        listCard.add(listCards.remove(0));
        listCard.add(listCards.remove(0));
        listCard.add(listCards.remove(0));
        return listCard;
    }

    /**
     * Method that add all the valid Truco cards to the listCards, and shuffle the listCards.
     * */
    private void shuffleCards() {

        listCards.add(new Card(CardValue.ACE, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.ACE, CardSuit.SPADES));
        listCards.add(new Card(CardValue.ACE, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.ACE, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.TWO, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.TWO, CardSuit.SPADES));
        listCards.add(new Card(CardValue.TWO, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.TWO, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.THREE, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.THREE, CardSuit.SPADES));
        listCards.add(new Card(CardValue.THREE, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.THREE, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.FOUR, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.FOUR, CardSuit.SPADES));
        listCards.add(new Card(CardValue.FOUR, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.FOUR, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.FIVE, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.FIVE, CardSuit.SPADES));
        listCards.add(new Card(CardValue.FIVE, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.FIVE, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.SIX, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.SIX, CardSuit.SPADES));
        listCards.add(new Card(CardValue.SIX, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.SIX, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.SEVEN, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.SEVEN, CardSuit.SPADES));
        listCards.add(new Card(CardValue.SEVEN, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.SEVEN, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.QUEEN, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.QUEEN, CardSuit.SPADES));
        listCards.add(new Card(CardValue.QUEEN, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.QUEEN, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.JACK, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.JACK, CardSuit.SPADES));
        listCards.add(new Card(CardValue.JACK, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.JACK, CardSuit.CLUBS));

        listCards.add(new Card(CardValue.KING, CardSuit.DIAMONDS));
        listCards.add(new Card(CardValue.KING, CardSuit.SPADES));
        listCards.add(new Card(CardValue.KING, CardSuit.HEARTS));
        listCards.add(new Card(CardValue.KING, CardSuit.CLUBS));

        // Shuffle function
        Collections.shuffle(listCards, new Random(System.nanoTime()));
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public Card getTurnedCard() {
        return turnedCard;
    }

    public void setTurnedCard(Card turnedCard) {
        this.turnedCard = turnedCard;
    }

    public List<Card> getListCards() {
        return listCards;
    }

    public void setListCards(List<Card> listCards) {
        this.listCards = listCards;
    }

    public void setNextPlayer(Team possibleWinner) {
        if (possibleWinner == team2) {
            nextPlayer = 2;
        }
        else {
            nextPlayer = 1;
        }
    }

    public void setNextPlayer(int nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public int getPointsForRound() {
        return pointsForRound;
    }

    public void setPointsForRound(int pointsForRound) {
        this.pointsForRound = pointsForRound;
    }

    public int getNextPlayerAux() {
        return nextPlayerAux;
    }

    public void setNextPlayerAux(int nextPlayerAux) {
        this.nextPlayerAux = nextPlayerAux;
    }

}
