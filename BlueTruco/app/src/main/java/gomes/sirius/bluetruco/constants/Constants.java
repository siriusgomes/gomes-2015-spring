package gomes.sirius.bluetruco.constants;

import java.util.UUID;

/**
 * Created by Sírius on 4/2/2015.
 */
public class Constants {


    public static final String GAME_MODE = "GAME_MODE";
    public static final String SINGLE_PLAYER_MODE = "SINGLE_PLAYER_MODE";
    public static final String MULTI_PLAYER_MODE_SERVER = "MULTI_PLAYER_MODE_SERVER";
    public static final String MULTI_PLAYER_MODE_CLIENT = "MULTI_PLAYER_MODE_CLIENT";

    public static final String INTENT_BROADCAST_NEXT_PLAYER = "INTENT_BROADCAST_NEXT_PLAYER";
    public static final String INTENT_BROADCAST_NEXT_ROUND = "INTENT_BROADCAST_NEXT_ROUND";
    public static final String INTENT_BROADCAST_SOCKET_INPUT = "INTENT_BROADCAST_SOCKET_INPUT";
    public static final String INTENT_BROADCAST_ANSWER_TRUCO = "INTENT_BROADCAST_ANSWER_TRUCO";

    public static final long DELAY_CPU = 1000;
    public static final long DELAY_NOTIFY = 500;
    public static final long DELAY_ANIMATION = 750;

    public static final int PLAYER_TYPE_REAL = 1;
    public static final int PLAYER_TYPE_CPU = 2;

    public static final int POINTS_TO_WIN = 12;

    public static final int BUFFER_SIZE= 1024;

    public static final int toleranzW = 1;
    public static final int toleranzH = 1;
    public static final int cardW = 196;
    public static final int cardH = 306;
    public static final int xTranslateAnimation = 20;
    public static final int distanceBetweenCardsThrowedMDPI = 45;
    public static final int distanceBetweenCardsThrowedXHDPI = 100;
    public static final int distanceBetweenCardsThrowedXXHDPI = 150;

    public static final String USER_NAME = "USER_NAME";

    public static final String socketName = "socketBlueTruco";
    public static final UUID socketUUIDServer = UUID.fromString("197ef340-e9ff-11e4-b571-0800200c9a66");
    public static final UUID socketUUIDClient = UUID.fromString("197ef341-e9ff-11e4-b571-0800200c9a66");

    public static final String JSON_PLAYER_NAME = "PLAYER_NAME";
    public static final String JSON_CARDS = "CARDS";
    public static final String JSON_CARD_TURNED = "TURNED_CARD";
    public static final String JSON_PLAYER1 = "PLAYER1";
    public static final String JSON_PLAYER2 = "PLAYER2";
    public static final String JSON_SCORE_TEAM1 = "JSON_SCORE_TEAM1";
    public static final String JSON_SCORE_TEAM2 = "JSON_SCORE_TEAM2";
    public static final String JSON_SCORE_ROUND_TEAM1 = "JSON_SCORE_ROUND_TEAM1";
    public static final String JSON_SCORE_ROUND_TEAM2 = "JSON_SCORE_ROUND_TEAM2";
    public static final String JSON_CARD_PLAYED = "JSON_CARD_PLAYED";
    public static final String JSON_WINNER = "JSON_WINNER";
    public static final String JSON_TRUCO_VALUE = "JSON_TRUCO_VALUE";


    public static final String SOCKET_PACKAGE_TYPE = "SOCKET_PACKAGE_TYPE";
    public static final String SOCKET_SERVER_NEW_TURN = "SOCKET_SERVER_NEW_TURN";
    public static final String SOCKET_SERVER_NEW_GAME = "SOCKET_SERVER_NEW_GAME";
    public static final String SOCKET_SERVER_PLAY_CARD = "SOCKET_SERVER_PLAY_CARD";
    public static final String SOCKET_CLIENT_PLAY_CARD = "SOCKET_CLIENT_PLAY_CARD";
    public static final String SOCKET_SERVER_ASK_TRUCO = "SOCKET_SERVER_ASK_TRUCO";
    public static final String SOCKET_CLIENT_ASK_TRUCO = "SOCKET_CLIENT_ASK_TRUCO";
    public static final String SOCKET_SERVER_ACCEPT_TRUCO = "SOCKET_SERVER_ACCEPT_TRUCO";
    public static final String SOCKET_SERVER_DECLINE_TRUCO = "SOCKET_SERVER_DECLINE_TRUCO";
    public static final String SOCKET_CLIENT_ACCEPT_TRUCO = "SOCKET_CLIENT_ACCEPT_TRUCO";
    public static final String SOCKET_CLIENT_DECLINE_TRUCO = "SOCKET_CLIENT_DECLINE_TRUCO";
    public static final String SOCKET_CLIENT_HANDSHAKE = "SOCKET_CLIENT_HANDSHAKE";
    public static final String SOCKET_SERVER_NOTIFY_WINNER_TO_CLIENT = "SOCKET_SERVER_NOTIFY_WINNER_TO_CLIENT";
    public static final String SOCKET_SERVER_NOTIFY_CLIENT_TO_PLAY = "SOCKET_SERVER_NOTIFY_CLIENT_TO_PLAY";
    public static final String SOCKET_CLIENT_NOTIFY_SERVER_TO_PLAY = "SOCKET_CLIENT_NOTIFY_SERVER_TO_PLAY";

}
