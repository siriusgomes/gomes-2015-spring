package gomes.sirius.bluetruco.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sírius on 3/31/2015.
 */
public class Team {

    private List<Player> players = new ArrayList<Player>();

    private int teamNumber;
    private int points;

    @Override
    public String toString() {
        return this.teamNumber + "";// + " with " + players.size() + " players";
    }

    public Team(int teamNumber, Player ... players) {
        for (int i = 0; i < players.length; i++) {
            players[i].setTeam(this);
            this.players.add(players[i]);
        }
        points = 0;
        this.teamNumber = teamNumber;
    }

    public Team(int teamNumber, List<Player> listPlayers) {
        for (Player player : listPlayers) {
            player.setTeam(this);
            this.players.add(player);
        }
        points = 0;
        this.teamNumber = teamNumber;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public int getPoints() {
        return this.points;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void setPlayers(Player player) {
        this.players.add(player);
    }

}
