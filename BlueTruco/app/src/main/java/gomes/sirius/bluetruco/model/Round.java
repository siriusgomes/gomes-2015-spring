package gomes.sirius.bluetruco.model;

/**
 * Created by Sírius on 4/2/2015.
 */
public class Round {
    private Player player;
    private Card card;

    public Round(Player player, Card card) {
        this.player = player;
        this.card = card;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
