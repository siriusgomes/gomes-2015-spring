package gomes.sirius.bluetruco.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import gomes.sirius.bluetruco.R;
import gomes.sirius.bluetruco.constants.Constants;


public class OptionsMenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options_menu);

        SharedPreferences settings = getSharedPreferences(Constants.USER_NAME, MODE_PRIVATE);

        final EditText editTextNameUser = (EditText) findViewById(R.id.playerName);
        editTextNameUser.setText(settings.getString(Constants.USER_NAME, ""));

        Button buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("OptionsMenuActivity", "onClickButtonSave");
                SharedPreferences settings = getSharedPreferences(Constants.USER_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(Constants.USER_NAME, editTextNameUser.getText().toString());
                editor.commit();
                Toast.makeText(OptionsMenuActivity.this, "Save!", Toast.LENGTH_LONG).show();
                finishActivity(0);
            }
        });
    }

}
