package gomes.sirius.bluetruco.model;

/**
 * Created by Sírius on 3/31/2015.
 */
public enum CardValue {
    ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, QUEEN, JACK, KING, GENERIC;
}