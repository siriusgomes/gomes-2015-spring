package gomes.sirius.bluetruco.model;

import org.json.JSONObject;
/**
 * Created by Sírius on 4/14/2015.
 */
public interface BasicJsonSerializable<T> {

    public JSONObject toJsonObject();

    public T fromJsonObject(JSONObject jsonObject);

}
