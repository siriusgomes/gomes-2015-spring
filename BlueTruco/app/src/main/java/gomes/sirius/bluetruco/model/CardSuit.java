package gomes.sirius.bluetruco.model;

/**
 * Created by Sírius on 3/31/2015.
 */
public enum CardSuit {
    SPADES,HEARTS,DIAMONDS,CLUBS;
}