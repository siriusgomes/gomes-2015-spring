package gomes.sirius.bluetruco.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import gomes.sirius.bluetruco.constants.Constants;

/**
 * Created by Sírius on 3/31/2015.
 */
public class Player implements BasicJsonSerializable {

    private String name;
    private List<Card> cards;
    private Team team;
    private int playerType; // 1 - Real Player. 2 - CPU Player.

    public Player(String name, int playerType) {
        this.name = name;
        this.playerType = playerType;
    }

    public Player() {

    }

    @Override
    public String toString() {
        return this.name + " from team " + team.toString();
    }

    public void debugPlayer() {
        for (Card card : cards) {
            Log.d("Player "+name+" from team "+team.toString()+" cards: ", card.toString());
        }
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public int getPlayerType() {
        return playerType;
    }

    public void setPlayerType(int playerType) {
        this.playerType = playerType;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.JSON_PLAYER_NAME, name);

            JSONArray jsonArrayCards = new JSONArray();

            for (Card card : cards) {
                jsonArrayCards.put(card.toJsonObject());
            }

            jsonObject.put(Constants.JSON_CARDS, jsonArrayCards);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public Player fromJsonObject(JSONObject jsonObject) {
        Player player = null;
        try {
           player = new Player(jsonObject.getString(Constants.JSON_PLAYER_NAME),1);
            List<Card> listCards = new ArrayList<Card>();
            JSONArray jsonArrayCards = jsonObject.getJSONArray(Constants.JSON_CARDS);

            Card cardFactor = new Card();

            listCards.add(cardFactor.fromJsonObject((JSONObject)jsonArrayCards.get(0)));
            listCards.add(cardFactor.fromJsonObject((JSONObject)jsonArrayCards.get(1)));
            listCards.add(cardFactor.fromJsonObject((JSONObject)jsonArrayCards.get(2)));

            player.setCards(listCards);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return player;
    }
}
