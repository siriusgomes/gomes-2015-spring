package gomes.sirius.bluetruco.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

import gomes.sirius.bluetruco.R;
import gomes.sirius.bluetruco.constants.Constants;
import gomes.sirius.bluetruco.controller.BlueTrucoSocket;
import gomes.sirius.bluetruco.model.Player;


public class JoinMultiplayerActivity extends Activity {

    private BluetoothAdapter BA;
    private BluetoothDevice BD;
    private Set<BluetoothDevice> pairedDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_multiplayer);

        Log.d("JoinMultiplayerActivity - onResume", "Setting socket..");

        Button buttonStartGame = (Button) findViewById(R.id.buttonStartGameJoin);
        buttonStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent singlePlayer = new Intent(JoinMultiplayerActivity.this, StartActivity.class);
                singlePlayer.putExtra(Constants.GAME_MODE,Constants.MULTI_PLAYER_MODE_CLIENT);
                startActivity(singlePlayer);
            }
        });

        if (BlueTrucoSocket.socket == null || BlueTrucoSocket.socket != null && !BlueTrucoSocket.socket.isConnected()) {
            new SocketConnect().execute();
        }
    }



    private class SocketConnect extends AsyncTask<String, Integer, Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Enabling the bluetooth interface.
            JoinMultiplayerActivity.this.BA = BluetoothAdapter.getDefaultAdapter();
            if (!BA.isEnabled()) {
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnOn, 0); // todo try to show an error in case of activity returning error...
            }

            // Disabling button to avoid clicking on it.
            Button buttonStartGame = (Button) findViewById(R.id.buttonStartGameJoin);
            buttonStartGame.setVisibility(View.INVISIBLE);

            Log.d("SocketConnect - preExecute", "Enabling bluetooth Interface and discover mode");
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                // Enabling the bluetooth interface.
                BA = BluetoothAdapter.getDefaultAdapter();

                // Getting list of paired devices..
                pairedDevices = BA.getBondedDevices();

                // Setting macaddress of the paired device //todo I need do add some dynamism on it...
                String macAddressServer = pairedDevices.iterator().next().getAddress();
                BD = BA.getRemoteDevice(macAddressServer);

                // Establishing connection
                BluetoothSocket clientSocket = BD.createRfcommSocketToServiceRecord(Constants.socketUUIDClient);
                //BluetoothSocket clientSocket = BD.createInsecureRfcommSocketToServiceRecord(Constants.socketUUID);
                clientSocket.connect();

                // Creating first package (JsonObject with name of the remote player).
                JSONObject jsonObject = new JSONObject();
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.USER_NAME, MODE_PRIVATE);
                String userName = sharedPreferences.getString(Constants.USER_NAME, "Client");

                jsonObject.put(Constants.JSON_PLAYER_NAME, userName);
                jsonObject.put(Constants.SOCKET_PACKAGE_TYPE, Constants.SOCKET_CLIENT_HANDSHAKE);

                // Sending object..
                clientSocket.getOutputStream().write(jsonObject.toString().getBytes());
                clientSocket.getOutputStream().flush();

                BlueTrucoSocket.socket = clientSocket;

                Log.d("SocketConnect - doInBackground", "Sending first package to " + BlueTrucoSocket.socket.getRemoteDevice().getName() + ": " + jsonObject.toString());
            }
            catch (Exception e) {
                Log.e("SocketConnect - doInBackground", e.getMessage());
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            // Enabling button.
            Button buttonStartGame = (Button) findViewById(R.id.buttonStartGameJoin);
            buttonStartGame.setVisibility(View.VISIBLE);

            TextView textViewNameBluetooth = (TextView) findViewById(R.id.textViewNameBluetoothJoin);
            textViewNameBluetooth.setText("Smartphone Connected: " + BlueTrucoSocket.socket.getRemoteDevice().getName());

            Log.d("SocketConnect - postExecute", "Enabling startGame button and setting name from outgoing connection!");
        }
    }

}
