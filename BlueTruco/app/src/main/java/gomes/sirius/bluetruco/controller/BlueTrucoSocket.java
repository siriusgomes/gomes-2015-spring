package gomes.sirius.bluetruco.controller;

import android.bluetooth.BluetoothSocket;

import java.util.ArrayList;
import java.util.List;

import gomes.sirius.bluetruco.constants.Constants;

/**
 * Created by Sírius on 4/14/2015.
 */
public class BlueTrucoSocket {

    public static BluetoothSocket socket = null;

    public static List<byte[]> bufferInput = new ArrayList<>();

    public static List<byte[]> bufferOutput = new ArrayList<>();

}
