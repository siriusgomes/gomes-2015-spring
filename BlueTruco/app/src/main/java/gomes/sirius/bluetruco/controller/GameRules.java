package gomes.sirius.bluetruco.controller;

import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.util.List;

import gomes.sirius.bluetruco.constants.Constants;
import gomes.sirius.bluetruco.model.Card;
import gomes.sirius.bluetruco.model.CardSuit;
import gomes.sirius.bluetruco.model.Round;
import gomes.sirius.bluetruco.model.Team;

/**
 * Created by Sírius on 3/31/2015.
 */
public class GameRules {

    public static int[] turnsWinner;
    public static List<Round> listRound;


    /**
     * Function that will verify if there is a winner of the turn.
     * It will return 0 if there is no winner.
     * Or it will return 1 or 2 in case of team 1 or team 2 victory, respectively. */
    public static Integer verifyTurnWinner() {
        int winnerTeamNumber;

        // if first two are != -1, so we can already define a winner for the round...
        if (turnsWinner[0] != -1 && turnsWinner[1] != -1) {

            // if the winner of the first two turns is the same, and it's not tie, return this guy
            // if the first turn has a winner, and the second turn ties, return this guy too.
            if ((turnsWinner[0] == turnsWinner[1] && turnsWinner[0] != 0) || (turnsWinner[0] != 0 && turnsWinner[1] == 0)) {
                winnerTeamNumber = turnsWinner[0];
            }
            // if the first turn ties and the second turn has a winner, return this guy.
            else if ((turnsWinner[0] == 0 && turnsWinner[1] != 0)) {
                winnerTeamNumber = turnsWinner[1];
            }
            else {
                if (turnsWinner[2] != -1) {
                    if (turnsWinner[2] == 0) {
                        winnerTeamNumber = turnsWinner[0];
                    }
                    else {
                        winnerTeamNumber = turnsWinner[2];
                    }
                }
                else {
                    winnerTeamNumber = 0;
                }
            }
        }
        else {
            winnerTeamNumber = 0;
        }
        return winnerTeamNumber;
    }


    /**
     * Function that will set in the array turnsWinner the winner of the current round.
     * */
    public static void setTurnWinner(Round winner) {
        if (turnsWinner[0] < 0) {
            if (winner != null)
                turnsWinner[0] = winner.getPlayer().getTeam().getTeamNumber();
            else
                turnsWinner[0] = 0;
        }
        else if (turnsWinner[1] < 0) {
            if (winner != null)
                turnsWinner[1] = winner.getPlayer().getTeam().getTeamNumber();
            else
                turnsWinner[1] = 0;
        }
        else if (turnsWinner[2] < 0) {
            if (winner != null)
                turnsWinner[2] = winner.getPlayer().getTeam().getTeamNumber();
            else
                turnsWinner[2] = 0;
        }
    }

    /**
     *  Function that returns the winner for the round and clean all the cards in the screen.
     *  */
    public static Round getWinnerAndCleanAnimation(Card turnedCard) {
        Round winner = new Round(null, null);

        for (final Round round : listRound) {
            if (winner.getPlayer() == null && winner.getCard() == null) {
                winner.setPlayer(round.getPlayer());
                winner.setCard(round.getCard());
            }
            else {
                if (compareCards(winner.getCard(), round.getCard(), turnedCard) > 0) {
                    winner.setPlayer(round.getPlayer());
                    winner.setCard(round.getCard());
                }
                else if (compareCards(winner.getCard(), round.getCard(), turnedCard) == 0) {
                    winner = null;
                }
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    round.getCard().getCardImage().clearAnimation();
                    round.getCard().getCardImage().setVisibility(View.INVISIBLE);
                }
            }, Constants.DELAY_CPU);


            round.getCard().getCardImage().setOnClickListener(null);
            round.getPlayer().getCards().remove(round.getCard());

        }
        return winner;
    }

    public static boolean isManilha(Card turned, Card c1) {
        int manilhaValue = turned.getValue()+1;
        if (manilhaValue == 11) { // Means that the turned card was a 3, so the manilha is 4, which value is 1.
            manilhaValue = 1;
        }

        // This means c1 is Manilha, so let's set the new value for c1
        if (manilhaValue == c1.getValue()) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Function that will compare two cards.
     * The expected return values are:
     * -1 if c1 > c2
     * 0 if c1 == c2
     * 1 if c1 < c2
     * */
    public static int compareCards(Card c1, Card c2, Card turned) {
        int manilhaValue = turned.getValue()+1;
        if (manilhaValue == 11) { // Means that the turned card was a 3, so the manilha is 4, which value is 1.
            manilhaValue = 1;
        }

        // This means c1 is Manilha, so let's set the new value for c1
        if (manilhaValue == c1.getValue()) {
            c1.setValue(10 + getSuitValue(c1));
        }

        // This means c2 is Manilha, so let's set the new value for c2
        if (manilhaValue == c2.getValue()) {
            c2.setValue(10 + getSuitValue(c2));
        }

        Log.d("Player 1 card is ", c1.toString() + " - value is: " + c1.getValue());
        Log.d("Player 2 card is ", c2.toString() + " - value is: " + c2.getValue());

        if (c1.getValue() < c2.getValue()) {
            return 1;
        }
        else if (c1.getValue() > c2.getValue()) {
            return -1;
        }
        else {//(c1.getValue() == c2.getValue()) {
            return 0;
        }
    }

    /**
     * Function that will return a value for the suit of the card
     * Spades   = '♠'  - 2
     * Hearts   = '♥'  - 3
     * Diamonds = '♦'  - 1
     * Clubs    = '♣'  - 4
     */
    public static int getSuitValue(Card card) {
        if (card.getSuit() == CardSuit.CLUBS) {
            return 4;
        }
        else if (card.getSuit() == CardSuit.HEARTS) {
            return 3;
        }
        else if (card.getSuit() == CardSuit.SPADES) {
            return 2;
        }
        else if (card.getSuit() == CardSuit.DIAMONDS) {
            return 1;
        }
        return 0;
    }
}
