package gomes.sirius.bluetruco.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.ParcelUuid;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

import gomes.sirius.bluetruco.R;
import gomes.sirius.bluetruco.constants.Constants;
import gomes.sirius.bluetruco.controller.BlueTrucoSocket;


public class CreateMultiplayerActivity extends Activity {

    private BluetoothAdapter BA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_multiplayer);

        Log.d("CreateMultiplayerActivity - onCreate", "Setting socket..");

        Button buttonStartGame = (Button) findViewById(R.id.buttonStartGame);
        buttonStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent multiPlayerServer = new Intent(CreateMultiplayerActivity.this, StartActivity.class);
                multiPlayerServer.putExtra(Constants.GAME_MODE, Constants.MULTI_PLAYER_MODE_SERVER);
                startActivity(multiPlayerServer);
            }
        });

        if (BlueTrucoSocket.socket == null || BlueTrucoSocket.socket != null && !BlueTrucoSocket.socket.isConnected()) {
            new SocketAccept().execute();
        }

    }

    private class SocketAccept extends AsyncTask<String, Integer, Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Enabling the bluetooth interface.
            BA = BluetoothAdapter.getDefaultAdapter();
            if (!BA.isEnabled()) {
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnOn, 0); // todo try to show an error in case of activity returning error...
            }

            // Enabling the discover mode
            Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(getVisible, 0);

            // Disabling button to avoid clicking on it.
            Button buttonStartGame = (Button) findViewById(R.id.buttonStartGame);
            buttonStartGame.setVisibility(View.INVISIBLE);

            Log.d("SocketAccept - preExecute", "Enabling bluetooth Interface and discover mode");
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {

                BluetoothServerSocket serverSocket = BA.listenUsingRfcommWithServiceRecord(Constants.socketName, Constants.socketUUIDClient);
                //BluetoothServerSocket serverSocket = BA.listenUsingInsecureRfcommWithServiceRecord(Constants.socketName, Constants.socketUUID);
                BlueTrucoSocket.socket = serverSocket.accept();
                Log.d("SocketAccept - doInBackground", "new incoming connection from: " + BlueTrucoSocket.socket.getRemoteDevice().getName());
            }
            catch (Exception e) {
                Log.e("SocketAccept - doInBackground", e.getMessage());
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            // Enabling button.
            Button buttonStartGame = (Button) findViewById(R.id.buttonStartGame);
            buttonStartGame.setVisibility(View.VISIBLE);

            TextView textViewNameBluetooth = (TextView) findViewById(R.id.textViewNameBluetooth);
            textViewNameBluetooth.setText("Smartphone Connected: " + BlueTrucoSocket.socket.getRemoteDevice().getName());

            Log.d("SocketAccept - onPostExecute", "Enabling startGame button and setting name from incoming connection!");
        }
    }

}
