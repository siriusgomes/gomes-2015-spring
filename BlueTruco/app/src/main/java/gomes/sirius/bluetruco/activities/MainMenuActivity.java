package gomes.sirius.bluetruco.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import gomes.sirius.bluetruco.R;
import gomes.sirius.bluetruco.constants.Constants;


public class MainMenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_menu);


        Button singlePlayer = (Button) findViewById(R.id.buttonStartSinglePlayer);
        singlePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent singlePlayer = new Intent(MainMenuActivity.this, StartActivity.class);
                singlePlayer.putExtra(Constants.GAME_MODE,Constants.SINGLE_PLAYER_MODE);
                startActivity(singlePlayer);
            }
        });


        Button createMultiPlayerGame = (Button) findViewById(R.id.buttonStartMultiPlayer);
        createMultiPlayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createMultiPlayer = new Intent(MainMenuActivity.this, CreateMultiplayerActivity.class);
                //createMultiPlayer.putExtra(Constants.GAME_MODE, Constants.MULTI_PLAYER_MODE);
                startActivity(createMultiPlayer);
            }
        });


        Button joinMultiPlayerGame = (Button) findViewById(R.id.buttonJoinMultiPlayer);
        joinMultiPlayerGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent joinMultiPlayer = new Intent(MainMenuActivity.this, JoinMultiplayerActivity.class);
                //joinMultiPlayer.putExtra(Constants.GAME_MODE, Constants.MULTI_PLAYER_MODE);
                startActivity(joinMultiPlayer);
            }
        });


        Button optionsActivity = (Button) findViewById(R.id.buttonOptions);
        optionsActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent optionsActivity = new Intent(MainMenuActivity.this, OptionsMenuActivity.class);
                startActivity(optionsActivity);
            }
        });


        Button howToPlayActivity = (Button) findViewById(R.id.buttonHowToPlay);
        howToPlayActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent howToPlayActivity = new Intent(MainMenuActivity.this, HowToPlayActivity.class);
                //joinMultiPlayer.putExtra(Constants.GAME_MODE, Constants.MULTI_PLAYER_MODE);
                startActivity(howToPlayActivity);
            }
        });


    }

}
