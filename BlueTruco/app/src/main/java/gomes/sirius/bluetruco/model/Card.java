package gomes.sirius.bluetruco.model;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import gomes.sirius.bluetruco.R;
import gomes.sirius.bluetruco.constants.Constants;
import gomes.sirius.bluetruco.controller.Table;

/**
 * Created by Sírius on 3/31/2015.
 */
public class Card implements BasicJsonSerializable{

    private CardValue id;
    private CardSuit suit;
    private ImageView cardImage;
    private Bitmap realImageCard;


    private int value;

    static char A        = 'A';
    static char two      = '2';
    static char three    = '3';
    static char four     = '4';
    static char five     = '5';
    static char six      = '6';
    static char seven    = '7';
    static char queen    = 'Q';
    static char jack     = 'J';
    static char king     = 'K';

    static char suitSpades   = '♠';
    static char suitHearts   = '♥';
    static char suitDiamonds = '♦';
    static char suitClubs    = '♣';


    public Card(CardValue id, CardSuit suit) {
        this.id = id;
        this.suit = suit;
        this.setValue();
    }

    public Card() {

    }

    public CardValue getId() {
        return id;
    }

    public void setId(CardValue id) {
        this.id = id;
    }

    public CardSuit getSuit() {
        return suit;
    }

    public void setSuit(CardSuit suit) {
        this.suit = suit;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ImageView getCardImage() {
        return cardImage;
    }

    public void setCardImage(ImageView cardImage) {
        this.cardImage = cardImage;
    }

    public Bitmap getRealImageCard() {
        return realImageCard;
    }

    public void setRealImageCard(Bitmap realImageCard) {
        this.realImageCard = realImageCard;
    }

    @Override
    public boolean equals(Object o) {
        boolean isEquals = false;
        if (o instanceof Card) {
            isEquals = ((Card) o).id.equals(this.id) && ((Card) o).suit.equals(this.suit);
        }
        return isEquals;
    }

    public void cardAction(Table table, final Player player, boolean unrestricted) {

        Log.d("cardAction", table.getGameMode() + " - player " + player + " - next player" + table.getNextPlayer());

        switch(table.getGameMode()) {
            case Constants.MULTI_PLAYER_MODE_CLIENT:
                if (table.getNextPlayer() == 1 && (player.getTeam().getTeamNumber() == 1 || unrestricted)) {
                    this.getCardImage().setImageBitmap(this.getRealImageCard());
                    animateCard(this, table);
                    table.playRound(player, Card.this);
                }
                else if (table.getNextPlayer() == 2 && (player.getTeam().getTeamNumber() == 2 || unrestricted)) {
                    animateCard(this, table);
                    table.playRound(player, Card.this);
                }
            break;
            case Constants.SINGLE_PLAYER_MODE:
                animateCard(this, table);
                table.playRound(player, Card.this);
            break;
            case Constants.MULTI_PLAYER_MODE_SERVER:
                // If the current game-mode is server and it's time to the server to play...
                if (table.getNextPlayer() == 1 && (player.getTeam().getTeamNumber() == 1 || unrestricted)) {
                    animateCard(this, table);
                    table.playRound(player, Card.this);
                }
                else if (table.getNextPlayer() == 2 && (player.getTeam().getTeamNumber() == 2 || unrestricted)) {
                    this.getCardImage().setImageBitmap(this.getRealImageCard());
                    animateCard(this, table);
                    table.playRound(player, Card.this);
                }
            break;

        }








    }

    public void setCardAnimation(final Table table, final Player player) {

        this.cardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Card.this.cardAction(table,player, false);

              /*  TranslateAnimation translateAnimation = null;
                if (player.getTeam().getTeamNumber() == 1) {
                    translateAnimation = new TranslateAnimation(0,50,0,-250);
                }
                else if (player.getTeam().getTeamNumber() == 2) {
                    translateAnimation = new TranslateAnimation(0,50,0,250);
                }

                translateAnimation.setDuration(1000);
                translateAnimation.setFillAfter(true);
                Card.this.cardImage.startAnimation(translateAnimation);
                Card.this.cardImage.setScaleX(0.6f);
                Card.this.cardImage.setScaleY(0.6f);
                table.playRound(player, Card.this);*/
            }
        });


    }

    private void animateCard(Card card, Table table) {
        TranslateAnimation translateAnimation = table.getTranslateAnimationForCard(this);

        translateAnimation.setDuration(Constants.DELAY_ANIMATION);
        translateAnimation.setFillAfter(true);
        Card.this.cardImage.setScaleX(0.6f);
        Card.this.cardImage.setScaleY(0.6f);
        Card.this.cardImage.startAnimation(translateAnimation);
    }


    private void setValue() {
        switch (id) {
            case ACE:
                value = 8;
                break;
            case TWO:
                value = 9;
                break;
            case THREE:
                value = 10;
                break;
            case FOUR:
                value = 1;
                break;
            case FIVE:
                value = 2;
                break;
            case SIX:
                value = 3;
                break;
            case SEVEN:
                value = 4;
                break;
            case QUEEN:
                value = 5;
                break;
            case JACK:
                value = 6;
                break;
            case KING:
                value = 7;
                break;
        }
    }

    public Bitmap getCardBitmap(Bitmap icon, float factor, float scale) {
        int x;
        int y;//imageButton.setImageResource(R.drawable.cards);

        x=(int) (getCardPosition().get('x')*factor);
        y=(int) (getCardPosition().get('y')*factor);
        Bitmap result = Bitmap.createBitmap((int)((Constants.cardW*factor)*scale), (int)((Constants.cardH*factor)*scale), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(icon, new Rect(x, y, x + (int)(Constants.cardW*factor) + Constants.toleranzW, y + (int)(Constants.cardH*factor) + Constants.toleranzH), new Rect(0, 0, (int)((Constants.cardW*factor)*scale), (int)((Constants.cardH*factor)*scale)), new Paint());
        return result;
    }

    public Bitmap getCardBitmap(Bitmap icon, float factor) {
       return getCardBitmap(icon,factor,1);
    }

    public static Bitmap getGenericCardBitmap(Bitmap icon, float factor, float scale) {
        return new Card(CardValue.GENERIC, null).getCardBitmap(icon,factor, scale);
    }

    public static Bitmap getGenericCardBitmap(Bitmap icon, float factor) {
        return new Card(CardValue.GENERIC, null).getCardBitmap(icon,factor, 1);
    }


    private Map<Character, Integer> getCardPosition() {
        Map<Character, Integer> mapPosition = new HashMap<Character, Integer>();

        if (id == CardValue.ACE) {
            mapPosition.put('x',0);
        }
        else if (id == CardValue.TWO) {
            mapPosition.put('x',197);
        }
        else if (id == CardValue.THREE) {
            mapPosition.put('x',394);
        }
        else if (id == CardValue.FOUR) {
            mapPosition.put('x',591);
        }
        else if (id == CardValue.FIVE) {
            mapPosition.put('x',788);
        }
        else if (id == CardValue.SIX) {
            mapPosition.put('x',985);
        }
        else if (id == CardValue.SEVEN) {
            mapPosition.put('x',1182);
        }
        else if (id == CardValue.QUEEN) {
            mapPosition.put('x',2166);
        }
        else if (id == CardValue.JACK) {
            mapPosition.put('x',1969);
        }
        else if (id == CardValue.KING) {
            mapPosition.put('x',2363);
        }
        else if (id == CardValue.GENERIC) {
            mapPosition.put('x', 394);
            mapPosition.put('y', 1225);
        }

        if (suit == CardSuit.DIAMONDS) {
            mapPosition.put('y',306);
        }
        else if (suit == CardSuit.SPADES) {
            mapPosition.put('y', 919);
        }
        else if (suit == CardSuit.HEARTS) {
            mapPosition.put('y',612);
        }
        else if (suit == CardSuit.CLUBS) {
            mapPosition.put('y',0);
        }
        return mapPosition;
    }


    @Override
    public String toString() {
        String card = "";
        switch (id) {
            case ACE:
                card += String.valueOf(A);
                break;
            case TWO:
                card += String.valueOf(two);
                break;
            case THREE:
                card += String.valueOf(three);
                break;
            case FOUR:
                card += String.valueOf(four);
                break;
            case FIVE:
                card += String.valueOf(five);
                break;
            case SIX:
                card += String.valueOf(six);
                break;
            case SEVEN:
                card += String.valueOf(seven);
                break;
            case QUEEN:
                card += String.valueOf(queen);
                break;
            case JACK:
                card += String.valueOf(jack);
                break;
            case KING:
                card += String.valueOf(king);
                break;
        }

        switch (suit) {
            case SPADES:
                card += String.valueOf(suitSpades);
                break;
            case HEARTS:
                card += String.valueOf(suitHearts);
                break;
            case DIAMONDS:
                card += String.valueOf(suitDiamonds);
                break;
            case CLUBS:
                card += String.valueOf(suitClubs);
                break;

        }
        return card;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id.toString());
            jsonObject.put("suit", suit.toString());
            //jsonObject.put("cardImage", cardImage);
            //jsonObject.put("realImageCard", realImageCard);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public Card fromJsonObject(JSONObject jsonObject) {
        try {
            return new Card(CardValue.valueOf(jsonObject.get("id").toString()), CardSuit.valueOf(jsonObject.get("suit").toString()));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


}




