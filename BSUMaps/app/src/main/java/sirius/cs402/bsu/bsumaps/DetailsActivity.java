package sirius.cs402.bsu.bsumaps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import sirius.cs402.bsu.constants.Constants;
import sirius.cs402.bsu.controller.BuildingsController;
import sirius.cs402.bsu.model.Building;


public class DetailsActivity extends ActionBarActivity {

    Building actualBuilding;
    BuildingsController buildingsController;
    private boolean isEdit =  false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        buildingsController = new BuildingsController(this);

        // Just receiving the coordinates from the MapViewIntent, setting it in a new Building Obj..
        try {

            // New building, so let's just set the LatLng
            if (getIntent().getStringExtra(Constants.buildingNew) != null) {
                JSONObject newBuildingLocationJSON = new JSONObject(getIntent().getStringExtra(Constants.buildingNew));
                double newBuildingLatitude = newBuildingLocationJSON.getDouble(Constants.jsonLatitude);
                double newBuildingLongitude = newBuildingLocationJSON.getDouble(Constants.jsonLongitude);
                actualBuilding = new Building(new LatLng(newBuildingLatitude, newBuildingLongitude));
                TextView textViewLatLng = (TextView) findViewById(R.id.textViewDetailsLatLng);
                textViewLatLng.setText(newBuildingLocationJSON.toString());
            }
            // Existing building, so let's set everything
            else if (getIntent().getStringExtra(Constants.buildingEdit) != null) {
                isEdit = true;
                JSONObject newBuildingLocationJSON = new JSONObject(getIntent().getStringExtra(Constants.buildingEdit));
                double newBuildingLatitude = newBuildingLocationJSON.getDouble(Constants.jsonLatitude);
                double newBuildingLongitude = newBuildingLocationJSON.getDouble(Constants.jsonLongitude);
                actualBuilding = buildingsController.getBuilding(new LatLng(newBuildingLatitude, newBuildingLongitude));
                TextView textViewLatLng = (TextView) findViewById(R.id.textViewDetailsLatLng);
                textViewLatLng.setText(newBuildingLocationJSON.toString());
                TextView textViewBuildingTitle = (TextView) findViewById(R.id.buildingTitle);
                textViewBuildingTitle.setText(actualBuilding.getName());
                TextView textViewBuildingDescription = (TextView) findViewById(R.id.buildingDescription);
                textViewBuildingDescription.setText(actualBuilding.getDescription());
                if (actualBuilding.getImage() != null) {
                    ImageView imageViewBuildingPicture = (ImageView) findViewById(R.id.building_picture);
                    imageViewBuildingPicture.setImageBitmap(actualBuilding.getImage());
                }
            }

        } catch (JSONException e) {
            Log.e("Error getting JSONObj from intent DetailsActivity", e.toString());
        }


        // Listener for the add picture button..
        ImageButton cameraButton = (ImageButton)findViewById(R.id.buildingAddPictureButton);
        cameraButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, Constants.PictureIntentCode);
            }
        });


        // Listener for the save button
        Button saveButton = (Button) findViewById(R.id.buttonSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText title = (EditText)findViewById(R.id.buildingTitle);
                EditText description = (EditText)findViewById(R.id.buildingDescription);
                ImageView imageView = (ImageView)findViewById(R.id.building_picture);

                actualBuilding.setName(title.getText().toString());
                actualBuilding.setDescription(description.getText().toString());

                if ((imageView.getDrawable()) != null)
                    actualBuilding.setImage(((BitmapDrawable)imageView.getDrawable()).getBitmap());


                if (!isEdit) {
                    buildingsController.addBuilding(actualBuilding);
                }
                else {
                    buildingsController.updateBuilding(actualBuilding);
                }

                actualBuilding = null;
                Toast.makeText(DetailsActivity.this, getString(R.string.message_created_marker), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

 /*       final ImageView imageViewPicture = (ImageView) findViewById(R.id.building_picture);
        imageViewPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(zoomImage) {
                    Toast.makeText(getApplicationContext(), "NORMAL SIZE!", Toast.LENGTH_LONG).show();
                    imageViewPicture.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    imageViewPicture.setAdjustViewBounds(true);
                    zoomImage =false;
                }else{
                    Toast.makeText(getApplicationContext(), "FULLSCREEN!", Toast.LENGTH_LONG).show();
                    imageViewPicture.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    imageViewPicture.setScaleType(ImageView.ScaleType.FIT_XY);
                    zoomImage = true;
                }
            }

        });
*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.PictureIntentCode && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap buildingPicture = (Bitmap) extras.get("data");

            ImageView imageView = (ImageView)findViewById(R.id.building_picture);
            imageView.setImageBitmap( buildingPicture );
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
