package sirius.cs402.bsu.model;

import android.graphics.Bitmap;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

import sirius.cs402.bsu.constants.Constants;

/**
 * Created by Sírius on 2/22/2015.
 */
public class Building implements Serializable {
    /*    {
    "name":"Morrison Center",
    "location": { "latitude":43.60683334, "longitude":-116.2075746 },
    "description":"Wood turf"
    }*/

    private int id;
    private String name;
    private LatLng location;
    private String description;
    private Bitmap image;

    @Override
    public String toString() {
        return "Name: " + this.name + "\nDescription: " + this.description + "\nLatitude: " + this.location.latitude + "\nLongitude: " + this.location.longitude;
    }
    public Building (){

    }
  /*  public Building(JSONObject jsonObject) {
        try {
            if (jsonObject != null) {
                this.name = jsonObject.getString(Constants.jsonName);
                this.description = jsonObject.getString(Constants.jsonDescription);

                JSONObject locationObj = jsonObject.getJSONObject(Constants.jsonLocation);
                if (locationObj != null) {
                    this.location = new LatLng(locationObj.getDouble(Constants.jsonLatitude), locationObj.getDouble(Constants.jsonLongitude));
                }
            }
        } catch (JSONException e) {
            Log.d("Error setting Location obj from JSON Obj:", e.toString());
        }
    }*/

    public Building(Marker marker) {
        this.location = marker.getPosition();
    }

    public Building(LatLng latLng) {
        this.location = latLng;
    }


    public JSONObject getJSON() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Constants.jsonName, this.name);
            jsonObject.put(Constants.jsonDescription, this.description);
            JSONObject location = new JSONObject();
            location.put(Constants.jsonLatitude, this.location.latitude);
            location.put(Constants.jsonLongitude, this.location.longitude);
            jsonObject.put(Constants.jsonLocation, location);
        } catch (JSONException e) {
            Log.d("Error getting JSON Obj from Location:", e.toString());
        }

        return jsonObject;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getLocationFormatted() {
        return "Latitude: " + this.location.latitude + "\nLongitude: " + this.location.longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public byte[] getImageAsByteArray() {
        if (image != null) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
            return outputStream.toByteArray();
        }
        return null;
    }

    @Override
    public boolean equals(Object another) {
        if (another instanceof Building) {
            if (this.location.latitude == ((Building) another).location.latitude)
                if (this.location.longitude == ((Building) another).location.longitude)
                    return true;
           /* if (this.id == ((Building) another).id)
                return true;*/
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
