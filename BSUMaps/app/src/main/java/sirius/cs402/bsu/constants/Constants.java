package sirius.cs402.bsu.constants;

/**
 * Created by Sírius on 2/21/2015.
 */
public class Constants {

    // General constants for HTTP Request Obj
    public static final String HOST = "http://zstudiolabs.com";
    public static final String JSon_URL = "/labs/cs402/buildings.json";
    public static final int TIMEOUT = 30000;

    // JSON Fields constants


    public static final int PictureIntentCode = 1;
    public static final int AddingNewBuildingCode = 2;
    public static final int ReturningNewBuildingCode = 3;
    public static final int EditingBuildingCode = 4;


    /*    {
    "name":"Morrison Center",
    "location": { "latitude":43.60683334, "longitude":-116.2075746 },
    "description":"Wood turf"
    }*/


    public static final String newBuildingEventName = "newBuildingEvent";
    public static final String jsonDescription = "description";
    public static final String jsonLocation = "location";
    public static final String jsonName = "name";
    public static final String jsonLatitude = "latitude";
    public static final String jsonLongitude = "longitude";
    public static final String jsonExtraName = "listLocation";
    public static final String buildingNew = "newBuilding";
    public static final String centralizingBuilding = "centralizingBuilding";
    public static final String buildingEdit = "editBuilding";


}
