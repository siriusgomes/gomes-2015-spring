package sirius.cs402.bsu.bsumaps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sirius.cs402.bsu.constants.Constants;
import sirius.cs402.bsu.controller.BuildingsController;
import sirius.cs402.bsu.controller.BuildingsFactory;
import sirius.cs402.bsu.controller.WebServices;
import sirius.cs402.bsu.model.Building;

public class MapView extends ActionBarActivity {

    BuildingsController buildingsController;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        buildingsController = new BuildingsController(this);

        // Calling the webService only one time to populate the static arraylist of buildings..
        if (buildingsController.isEmpty()) {
            WebServices getResourceTask = new WebServices(Constants.HOST, this);
            getResourceTask.execute(Constants.JSon_URL);
        }
        // Start setting the map based in the arrayList on BuildingsController..
        setUpMapIfNeeded();

        // Setting the LocalBroadcastManager that will receive the callback when new elements get inserted in the listBuildings.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(Constants.newBuildingEventName));

        // this centralization is not working as expected.. =(
        // If a intent with mapview is created, we are going to centralize the map in the position of the building that will come
        /*if (getIntent().getStringExtra(Constants.centralizingBuilding) != null) {
            try {
                JSONObject newBuildingLocationJSON = new JSONObject(getIntent().getStringExtra(Constants.centralizingBuilding));
                CameraUpdate center=CameraUpdateFactory.newLatLngZoom (new LatLng(newBuildingLocationJSON.getLong(Constants.jsonLatitude), newBuildingLocationJSON.getLong(Constants.jsonLongitude)), 13);
                mMap.moveCamera(center);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();

            // Populating map..
            populateMap();

            // Enables the user location...
            mMap.setMyLocationEnabled(true);

            // Defines the view of the infowindow that appears when you click over the marker.
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                // This will use the default window..
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    Building building = buildingsController.getBuilding(marker);

                    // Getting view from the layout file info_window_layout
                    View v = getLayoutInflater().inflate(R.layout.marker_info_window, null);

                    // Getting the position from the marker
                    LatLng latLng = building.getLocation();

                    // Getting reference to the TextView to set latitude
                    TextView markerLatitude = (TextView) v.findViewById(R.id.marker_info_latitude);

                    // Getting reference to the TextView to set longitude
                    TextView markerLongitude = (TextView) v.findViewById(R.id.marker_info_longitude);

                    // Getting reference to the TextView to set title
                    TextView markerTitle = (TextView) v.findViewById(R.id.marker_info_title);

                    // Getting reference to the TextView to set description
                    TextView markerDescription = (TextView) v.findViewById(R.id.marker_info_description);

                    // Getting reference to the TextView to set image
                    ImageView markerPicture = (ImageView) v.findViewById(R.id.marker_info_image_view);

                    // Setting the title
                    markerTitle.setText(building.getName());

                    // Setting the description
                    markerDescription.setText(building.getDescription());

                    // Setting the latitude
                    markerLatitude.setText("Latitude:" + latLng.latitude);

                    // Setting the longitude
                    markerLongitude.setText("Longitude:" + latLng.longitude);

                    // Setting the picture
                    markerPicture.setImageBitmap(building.getImage());
                    //BitmapFactory.decodeResource(getResources(), R.drawable.abc_textfield_search_material);

                    return v;
                }
            });

            // When you click over the infowindow of the marker, we will call the intent DetailsActivity, to show information about this marker..
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    callDetailsActivity(null, marker);
                }
            });

            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    callDetailsActivity(latLng, null);
                }
            });
        }
        else {
            populateMap();
        }
    }

    /**
     * Method to fulfil the map with markers based on the Buildings on listBuildings.
     * */
    private void populateMap() {
        // Let's remove all the old markers and add every marker again!
        mMap.clear();

        // Let's iterate in the list of buildings and add the markers.
        for (MarkerOptions marker : buildingsController.getMarkerOptionsListBuildings()) {
            mMap.addMarker(marker);
        }
    }

    /**
     * Instance of BroadcastReceiver that will implement the method onReceive. This method will be called when a new building gets inserted in the listBuildings
     * */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            populateMap();
        }
    };


    /**
     * This method will call the intent of DetailsActivity to create a new Building based on latLng */
    private void callDetailsActivity(LatLng latLng, Marker marker) {
        try {
            Intent addNewIntent = new Intent(this, DetailsActivity.class);
            if (marker != null) {
                JSONObject location = new JSONObject();
                location.put(Constants.jsonLatitude, marker.getPosition().latitude);
                location.put(Constants.jsonLongitude, marker.getPosition().longitude);
                addNewIntent.putExtra(Constants.buildingEdit, location.toString());
            }
            else if (latLng != null) {
                // Sending objects over intents is kind of hardworking, so I'm just using JSON.toString(), make the work easier.
                JSONObject location = new JSONObject();
                location.put(Constants.jsonLatitude, latLng.latitude);
                location.put(Constants.jsonLongitude, latLng.longitude);
                addNewIntent.putExtra(Constants.buildingNew, location.toString());
            }
            startActivityForResult(addNewIntent, Constants.AddingNewBuildingCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.AddingNewBuildingCode) {
            populateMap();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.button_switch_list_view:
                // Sending objects over intents is kind of hardworking, so I'm just using JSON.toString(), make the work easier.
                Intent listViewIntent = new Intent(MapView.this, ListViewClass.class);
                JSONArray jsonArray = new JSONArray();
                listViewIntent.putExtra(Constants.jsonExtraName, buildingsController.getJSONArrayBuildings().toString());
                startActivity(listViewIntent);
                return true;
            case R.id.button_add_new_from_map_view:
                if (mMap.getMyLocation() != null) {
                    // Add a marker in the current position
                    Location myLocation = mMap.getMyLocation();
                    double myLatitude = myLocation.getLatitude();
                    double myLongitude = myLocation.getLongitude();
                    callDetailsActivity(new LatLng(myLatitude,myLongitude), null);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
