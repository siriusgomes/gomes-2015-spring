package sirius.cs402.bsu.bsumaps;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sirius.cs402.bsu.constants.Constants;
import sirius.cs402.bsu.controller.BuildingsDBController;
import sirius.cs402.bsu.controller.BuildingsFactory;
import sirius.cs402.bsu.model.Building;


public class ListViewClass extends DetailsActivity {

    static BuildingsDBController buildingsDBController;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        buildingsDBController = new BuildingsDBController(this);
        populateListView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateListView();
    }

    private void populateListView() {
        try {
            String JSONArrayExtra = this.getIntent().getStringExtra(Constants.jsonExtraName);
            JSONArray jsonArray = new JSONArray(JSONArrayExtra);

            //List<Building> listBuildings = BuildingsFactory.createBuildingsFromJSON(jsonArray.toString());
            List<Building> listBuildings = buildingsDBController.readBuildings();

            // Construct the adapter using the simple list model and the listLocations created based in the JSON array obj.
            //ArrayAdapter<Building> adapter = new ArrayAdapter<Building>(this, android.R.layout.simple_list_item_1, listBuildings);
            BuildingsAdapter adapter = new BuildingsAdapter(this, R.id.list_item, listBuildings);

            listView = ((ListView) findViewById(R.id.listView));
            listView.setAdapter(adapter);

            // Registering this listView to the contextMenu that will be shown when the user give a long click in one item.
            registerForContextMenu(listView);


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (parent.getItemAtPosition(position) instanceof Building) {
                            Building building = (Building) parent.getItemAtPosition(position);

                            Intent addNewIntent = new Intent(ListViewClass.this, DetailsActivity.class);

                            JSONObject location = new JSONObject();
                            location.put(Constants.jsonLatitude, building.getLocation().latitude);
                            location.put(Constants.jsonLongitude, building.getLocation().longitude);
                            addNewIntent.putExtra(Constants.buildingEdit, location.toString());


                            startActivityForResult(addNewIntent, Constants.EditingBuildingCode);
                        }
                    } catch (Exception e) {
                        Log.e("Error calling intent DetailsActivity ", e.toString());
                    }

                }
            });


        } catch (JSONException e) {
            Log.e("Error converting JSONArray OBJ into listView", e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.EditingBuildingCode && resultCode == RESULT_OK) {
            populateListView();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_view, menu);
    }

    @Override
    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
        populateListView();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete_action:
                buildingsDBController.removeBuilding((Building)listView.getItemAtPosition((int)info.id));
                return true;
           /* case R.id.seeonmap_action:

                Intent mapViewIntent = new Intent(ListViewClass.this, MapView.class);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Constants.jsonLatitude, ((Building)listView.getItemAtPosition((int)info.id)).getLocation().latitude );
                    jsonObject.put(Constants.jsonLongitude, ((Building) listView.getItemAtPosition((int) info.id)).getLocation().longitude);
                    mapViewIntent.putExtra(Constants.centralizingBuilding, jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(mapViewIntent);

                return true;*/
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class BuildingsAdapter extends ArrayAdapter<Building>
    {
        Context mContext;
        int layoutResourceId;
        List<Building> listBuildings = new ArrayList<Building>();


        public BuildingsAdapter(Context mContext, int layoutResourceId, List<Building> listBuildings) {
            super(mContext, layoutResourceId, listBuildings);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.listBuildings = listBuildings;

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.list_item, parent, false);
                Log.i("TAG", "Created new convertView");
            }
            /*if( convertView == null ){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item, null);
                Log.i("TAG", "Created new convertView");
            }*/

            Log.i("TAG", "Setting up row for position: " + position);

            Building currentBuilding = getItem(position);

            TextView titleTextView = (TextView) convertView.findViewById(R.id.nameTextView);
            titleTextView.setText(currentBuilding.getName());

            TextView descriptionTextView = (TextView) convertView.findViewById(R.id.descriptionTextView);
            descriptionTextView.setText(currentBuilding.getDescription());

            TextView latLngTextView = (TextView) convertView.findViewById(R.id.latLngTextView);
            latLngTextView.setText(currentBuilding.getLocationFormatted());

            ImageView buildingImageView = (ImageView) convertView.findViewById(R.id.buildingImageView);
            buildingImageView.setImageBitmap(currentBuilding.getImage());

            return convertView;
        }
    }



}
