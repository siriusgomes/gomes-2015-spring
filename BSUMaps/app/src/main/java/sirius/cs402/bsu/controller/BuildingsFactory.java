package sirius.cs402.bsu.controller;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sirius.cs402.bsu.constants.Constants;
import sirius.cs402.bsu.model.Building;

/**
 * Created by Sírius on 2/22/2015.
 */
public class BuildingsFactory {


    /**
     * This method will receive a JSONObject String with this format:
     * {
     * "name":"Morrison Center",
     * "location": { "latitude":43.60683334, "longitude":-116.2075746 },
     * "description":"Wood turf"
     * }
     * and will convert it in a Building*/
    public static Building createBuildingFromJSON(String json) {
        Building newBuilding = null;

        try {
            JSONObject jsonObject = new JSONObject(json);

            if (jsonObject != null) {
                newBuilding = new Building();

                newBuilding.setName(jsonObject.getString(Constants.jsonName));
                newBuilding.setDescription(jsonObject.getString(Constants.jsonDescription));

                JSONObject locationObj = jsonObject.getJSONObject(Constants.jsonLocation);
                if (locationObj != null) {
                    newBuilding.setLocation(new LatLng(locationObj.getDouble(Constants.jsonLatitude), locationObj.getDouble(Constants.jsonLongitude)));
                }
            }
        } catch (JSONException e) {
            Log.d("Error converting String (JSON Obj) to Building:", e.toString());
        }
        return newBuilding;
    }


    /**
     * This method will receive a JSONArray String with this format:
     * [{
     * "name":"Morrison Center",
     * "location": { "latitude":43.60683334, "longitude":-116.2075746 },
     * "description":"Wood turf"
     * },{
     * "name":"Morrison Center",
     * "location": { "latitude":43.60683334, "longitude":-116.2075746 },
     * "description":"Wood turf"
     * }]
     * And it will convert it in a List of Buildings.
     */
    public static List<Building> createBuildingsFromJSON(String json) {
        List<Building> listBuilding = new ArrayList<Building>();
        try {
            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                listBuilding.add(createBuildingFromJSON(jsonArray.getJSONObject(i).toString()));
            }
        } catch (JSONException e) {
            Log.d("Error converting String (JSON Array) to List<Building>:", e.toString());
        }
        return listBuilding;
    }
}
