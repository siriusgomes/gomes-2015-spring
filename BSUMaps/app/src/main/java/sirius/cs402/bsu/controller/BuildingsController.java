package sirius.cs402.bsu.controller;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import sirius.cs402.bsu.constants.Constants;
import sirius.cs402.bsu.model.Building;
import sirius.cs402.bsu.model.BuildingsDBModel;

/**
 * Created by Sírius on 2/22/2015.
 */
public class BuildingsController {

    //private static List<Building> listBuildings = new ArrayList<Building>();
    BuildingsDBController buildingsDBController;

    public BuildingsController(Context context){
        if (context != null)
            buildingsDBController = new BuildingsDBController(context);
    }

    public boolean isEmpty() {
        //return /*listBuildings*/buildingsDBController.readBuildings().isEmpty();
        return buildingsDBController.isEmpty();
    }

    // I can't think in a better way to find a building now, sorry =(
    public Building getBuilding(Marker marker) {
        return getBuilding(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
    }

    public Building getBuilding(MarkerOptions markerOptions) {
        return getBuilding(new LatLng(markerOptions.getPosition().latitude, markerOptions.getPosition().longitude));
    }

    public Building getBuilding(LatLng latLng) {
       return buildingsDBController.getBuilding(latLng);

       /*
        for (Building building : buildingsDBController.readBuildings()) {
            if (building.getLocation().latitude == latLng.latitude && building.getLocation().longitude == latLng.longitude) {
                foundBuilding = building;
                break;
            }
        }
        return foundBuilding;*/
    }

    public JSONArray getJSONArrayBuildings() {
        JSONArray jsonArray = new JSONArray();

        for (Building building : buildingsDBController.readBuildings()/*listBuildings*/) {
            jsonArray.put(building.getJSON());
        }
        return jsonArray;
    }

    public List<MarkerOptions> getMarkerOptionsListBuildings() {
        List<MarkerOptions> listMarkersBuildings = new ArrayList<MarkerOptions>();

        for (Building building : buildingsDBController.readBuildings()/*listBuildings*/) {
            MarkerOptions newBuilding = new MarkerOptions();
            newBuilding.position(building.getLocation());
            newBuilding.title(building.getName());
            listMarkersBuildings.add(newBuilding);
        }
        return listMarkersBuildings;
    }

    public void addBuilding(Building building) {
        //this.listBuildings.add(building);
        buildingsDBController.addBuilding(building);

        // Just create a empty Intent with the name that we expect for a new Building in the list..
        Intent intent = new Intent(Constants.newBuildingEventName);
        LocalBroadcastManager.getInstance(null).sendBroadcast(intent);
    }

    public void removeBuilding(Building building) {
        //this.listBuildings.remove(building);
        buildingsDBController.removeBuilding(building);
    }


    public void updateBuilding(Building building) {
        //this.listBuildings.remove(building);
        buildingsDBController.updateBuilding(building);
    }

}
