package sirius.cs402.bsu.controller;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;

import sirius.cs402.bsu.bsumaps.MapView;
import sirius.cs402.bsu.constants.Constants;
import sirius.cs402.bsu.model.Building;

/**
 * Created by Sírius on 2/22/2015.
 */
public class WebServices extends AsyncTask<String, Integer, Boolean> {

        String hostURL;
        JSONArray jsonArray;
        BuildingsController buildingsController;

        public WebServices(String newHostURL, Context context) {
            super();
            this.hostURL = newHostURL;
            buildingsController = new BuildingsController(context);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String resourceURL = hostURL + params[0];

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, Constants.TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, Constants.TIMEOUT);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpGet httpGet = new HttpGet(resourceURL);
            try {
                BasicResponseHandler responseHandler = new BasicResponseHandler();
                String response = httpClient.execute(httpGet, responseHandler);
                jsonArray = new JSONArray(response);

                if (buildingsController.isEmpty()) {
                    for (Building building : BuildingsFactory.createBuildingsFromJSON(jsonArray.toString())) {
                        buildingsController.addBuilding(building);
                    }
                }

                Log.i("Downloading JSON:", response);
            }
            catch ( Exception exception) {
                Log.e("Error Downloading JSON", exception.toString());
            }

            Log.i("Done downloading JSON from:", "Inside Do In Background: " + resourceURL);
            return true;
        }


        // Called after the thread finish the job, then we set the markers in the positions specified by the JSON
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            // Just create a empty Intent with the name that we expect for the callback of the JSON download.
            Intent intent = new Intent(Constants.newBuildingEventName);
            LocalBroadcastManager.getInstance(null).sendBroadcast(intent);
        }
 }
