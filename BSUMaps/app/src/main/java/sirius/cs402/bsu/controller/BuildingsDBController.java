package sirius.cs402.bsu.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import sirius.cs402.bsu.model.Building;
import sirius.cs402.bsu.model.BuildingsDBModel;

/**
 * Created by Sírius on 3/16/2015.
 */
public class BuildingsDBController extends SQLiteOpenHelper {

    /* DATABASE INFO */
    private static final String DB_NAME = "bsu_maps";
    private static final int DB_VERSION = 1;

    public BuildingsDBController( Context context )
    {
        super( context, DB_NAME, null, DB_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        // Create Coordinates Table
        String coordinatesTableCreationQuery = "CREATE TABLE " + BuildingsDBModel.BuildingsDB.TABLE_COORDINATES + " (" +
                BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_ID + " integer primary key autoincrement, " +
                BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LATITUDE  + " real, " +
                BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LONGITUDE  + " real  " +
                ")";

        // Create Buildings Table
        String buildingTableCreationQuery = "CREATE TABLE " + BuildingsDBModel.BuildingsDB.TABLE_BUILDING + " (" +
                BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_ID + " integer primary key autoincrement, " +
                BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_NAME  + " varchar(255), " +
                BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_DESCRIPTION + " varchar(255), " +
                BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_COORDINATE_ID + " integer references " +
                BuildingsDBModel.BuildingsDB.TABLE_COORDINATES + " (" + BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_ID + ")," +
                BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_IMAGE + " blob " +
                ")";
        try{
            database.execSQL( coordinatesTableCreationQuery );
            database.execSQL( buildingTableCreationQuery );
            Log.d("CREATING DATABASE", coordinatesTableCreationQuery + "\n" + buildingTableCreationQuery);
        }
        catch ( Exception exception )
        {
            Log.e("CREATING DATABASE", exception.getMessage());
        }
        finally {
            getReadableDatabase().close();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch ( oldVersion )
        {
            case 1:
                // Adds new table Majors: db version 2
            case 2:
                // Adds new column 'times' to table Majors: db version 3
            case 3:
                // Removes 'times' from table Majors: db version 4
            default:
                // Unhandled migration
                break;
        }
    }

    public void addBuilding( Building building )
    {
        ContentValues contentValues = new ContentValues();
        LatLng latLngCoordinate = building.getLocation();
        contentValues.put( BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LATITUDE, latLngCoordinate.latitude );
        contentValues.put( BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LONGITUDE, latLngCoordinate.longitude );
        long coordinateID = getWritableDatabase().insert(BuildingsDBModel.BuildingsDB.TABLE_COORDINATES, null, contentValues);

        contentValues = new ContentValues();
        contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_NAME, building.getName());
        contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_DESCRIPTION, building.getDescription());
        contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_COORDINATE_ID, coordinateID);
        contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_IMAGE, building.getImageAsByteArray());
        long locationID = getWritableDatabase().insert( BuildingsDBModel.BuildingsDB.TABLE_BUILDING, null, contentValues);
    }

    public void removeBuilding( Building building )
    {
        String [] whereArguments = { String.valueOf(building.getLocation().latitude), String.valueOf(building.getLocation().longitude) };

        try {
            int delete = getWritableDatabase().delete(BuildingsDBModel.BuildingsDB.TABLE_COORDINATES,
                    BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LATITUDE + "=? and " +
                    BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LONGITUDE + "=? ",
                    whereArguments);
            Log.d("Deleting Location", "Number of deleted locations:" + delete);

            delete = getWritableDatabase().delete(BuildingsDBModel.BuildingsDB.TABLE_BUILDING,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_NAME + "=? and " +
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_DESCRIPTION + "=?",
                    new String[]{building.getName(),building.getDescription()});

            Log.d("Deleting Building", "Number of deleted buildings:" + delete);
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        finally {
            getReadableDatabase().close();
        }
    }



    public void updateBuilding( Building building )
    {
        String [] whereArguments = { String.valueOf(building.getId()) };

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_NAME, building.getName());
            contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_DESCRIPTION, building.getDescription());
            contentValues.put(BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_IMAGE, building.getImageAsByteArray());
            int rows = getWritableDatabase().update(BuildingsDBModel.BuildingsDB.TABLE_BUILDING, contentValues, BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_ID + "=?", whereArguments);

            Log.d("Updating Building", "Number of updated buildings:" + rows);
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        finally {
            getReadableDatabase().close();
        }
    }

    public boolean isEmpty()
    {
        boolean isEmpty = false;

        try {
            String rawQueryString = "SELECT COUNT(*) FROM `" + BuildingsDBModel.BuildingsDB.TABLE_BUILDING + "`";
            Cursor cursorResults = getReadableDatabase().rawQuery(rawQueryString, null);

            if (cursorResults.moveToFirst()) {
                do {
                    Building newBuilding = new Building();
                    if (cursorResults.getInt(0) == 0) {
                        isEmpty = true;
                    }
                } while (cursorResults.moveToNext());
            }
        }
        catch (Exception e) {
            Log.e("isEmpty", e.getMessage());
        }
        finally {
            getReadableDatabase().close();
        }
        return isEmpty;
    }

    public Building getBuilding(LatLng latLng) {
        Building buildingReturn = null;

        String[] whereArguments = {String.valueOf(latLng.latitude), String.valueOf(latLng.longitude)};

        try {
            String[] columns = new String[]{BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_NAME,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_DESCRIPTION,
                    BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LATITUDE,
                    BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LONGITUDE,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_IMAGE,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_ID
            };
            String rawQueryString = "SELECT " + columns[0] + "," + columns[1] + "," + columns[2] + "," + columns[3] + "," + columns[4] + "," + BuildingsDBModel.BuildingsDB.TABLE_BUILDING+"."+columns[5] + " FROM `" +
                    BuildingsDBModel.BuildingsDB.TABLE_BUILDING + "` INNER JOIN " + BuildingsDBModel.BuildingsDB.TABLE_COORDINATES +
                    " ON " + BuildingsDBModel.BuildingsDB.TABLE_BUILDING + "." + BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_COORDINATE_ID +
                    "=" + BuildingsDBModel.BuildingsDB.TABLE_COORDINATES + "." + BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_ID +
                    " WHERE " + BuildingsDBModel.BuildingsDB.TABLE_COORDINATES + "." + BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LATITUDE + "=? AND " +
                    BuildingsDBModel.BuildingsDB.TABLE_COORDINATES + "." + BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LONGITUDE + "=?";

            Cursor cursorResults = getReadableDatabase().rawQuery(rawQueryString, whereArguments);


            int index = 0;
            if (cursorResults.moveToFirst()) {
                do {
                    Building newBuilding = new Building();
                    newBuilding.setName(cursorResults.getString(0));
                    newBuilding.setDescription(cursorResults.getString(1));
                    newBuilding.setLocation(new LatLng(cursorResults.getDouble(2), cursorResults.getDouble(3)));
                    byte[] image = cursorResults.getBlob(4);
                    if (image != null)
                        newBuilding.setImage(BitmapFactory.decodeByteArray(image, 0, image.length));
                    newBuilding.setId(cursorResults.getInt(5));
                    Log.i("GETTING BUILDING", index++ + ": " + cursorResults.getString(0) + ", " + cursorResults.getString(1));

                    buildingReturn = newBuilding;
                } while (cursorResults.moveToNext());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        finally {
            getReadableDatabase().close();
            return buildingReturn;
        }
    }

    public ArrayList<Building> readBuildings() {
        ArrayList<Building> buildings = new ArrayList<Building>();
        try {
            String[] columns = new String[]{BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_NAME,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_DESCRIPTION,
                    BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LATITUDE,
                    BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_LONGITUDE,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_IMAGE,
                    BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_ID

            };
            String rawQueryString = "SELECT " + columns[0] + "," + columns[1] + "," + columns[2] + "," + columns[3] + "," + columns[4] + "," + BuildingsDBModel.BuildingsDB.TABLE_BUILDING + "." + columns[5] + " FROM `" +
                    BuildingsDBModel.BuildingsDB.TABLE_BUILDING + "` INNER JOIN " + BuildingsDBModel.BuildingsDB.TABLE_COORDINATES +
                    " ON " + BuildingsDBModel.BuildingsDB.TABLE_BUILDING + "." + BuildingsDBModel.BuildingsDB.COLUMN_BUILDING_COORDINATE_ID +
                    "=" + BuildingsDBModel.BuildingsDB.TABLE_COORDINATES + "." + BuildingsDBModel.BuildingsDB.COLUMN_COORDINATES_ID;
            Cursor cursorResults = getReadableDatabase().rawQuery(rawQueryString, null);
//                query(false, LocationsContract.LocationsEntry.TABLE_LOCATIONS, columns, null, null, null, null, null, null );


            int index = 0;
            if (cursorResults.moveToFirst()) {
                do {
                    Building newBuilding = new Building();
                    newBuilding.setName(cursorResults.getString(0));
                    newBuilding.setDescription(cursorResults.getString(1));
                    newBuilding.setLocation(new LatLng(cursorResults.getDouble(2), cursorResults.getDouble(3)));
                    byte[] image = cursorResults.getBlob(4);
                    if (image != null)
                        newBuilding.setImage(BitmapFactory.decodeByteArray(image, 0, image.length));
                    newBuilding.setId(cursorResults.getInt(5));
                    Log.i("TAG", index++ + ": " + cursorResults.getString(0) + ", " + cursorResults.getString(1));

                    buildings.add(newBuilding);
                } while (cursorResults.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            getReadableDatabase().close();
            return buildings;
        }
    }
}