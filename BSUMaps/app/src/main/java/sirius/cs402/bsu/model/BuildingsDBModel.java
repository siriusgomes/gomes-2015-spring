package sirius.cs402.bsu.model;

import android.provider.BaseColumns;

/**
 * Created by Sírius on 3/16/2015.
 */
public final class BuildingsDBModel {
   
    public BuildingsDBModel(){}

    public static abstract class BuildingsDB implements BaseColumns
    {
        /* BUILDINGS TABLE */
        public static final String TABLE_BUILDING                = "Buildings";
        public static final String COLUMN_BUILDING_ID            = "id";
        public static final String COLUMN_BUILDING_DESCRIPTION   = "description";
        public static final String COLUMN_BUILDING_NAME          = "name";
        public static final String COLUMN_BUILDING_COORDINATE_ID = "coordinate_id";
        public static final String COLUMN_BUILDING_IMAGE         = "image";

        /* COORDINATES TABLE */
        public static final String TABLE_COORDINATES                    = "Coordinates";
        public static final String COLUMN_COORDINATES_ID                = "id";
        public static final String COLUMN_COORDINATES_LATITUDE          = "latitude";
        public static final String COLUMN_COORDINATES_LONGITUDE         = "longitude";
    }

}
